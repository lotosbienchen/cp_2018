#!usr/bin/python
"""
Programm fuer das sechste Aufgabenblatt 'Quantenmechanik von 1D-Potentialen II'
Es wird ein Gauß'sches Wellenpaket in einem asymetrischen Doppelmuldenpotential
V(x)=x^4 - x^2 - A*x dynamisch zeitentwickelt.
===============================================================================
Der Startpunkt des Wellenpaketes kann mit einem linken Mausklick im Plotbereich
ausgewaehlt werden. Nach dem Klick wird das Betragsquadrat des zeitentwickelten
Wellenpaketes dynamisch als Funktion der Zeit in Hoehe des Energieerwartungs-
wertes gezeichnet. Außerdem ist im Plot das Doppelmuldenpotential und das
Betragsquadrat der Eigenfunktionen auf Hoehe der Eigenenergien dargestellt. Die
Darstellung wird dabei skaliert, um die Funktion besser erkennen zu koennen.
===============================================================================
Die verwendeten Parameter sind:
    L = 1.75, mit L gleich den Grenzen des betrachteten Intervalls x in [-L, L]
    N = 1000, mit N gleich der Matrixgroeße
    p_0 = 0.0, mit p_0 gleich dem mittleren Impuls
    A = 0.15, mit A gleich dem Faktor der Asymmetrie
    heff = 0.07, mit heff gleich dem effektiven plank'schen Wirkungsquant
    Dx = 0.1, mit Dx gleich der Breite des Wellenpaketes
    tmax = 200, mit tmax gleich der Zahl der Gesamtzeiteinheiten, ueber die
               entwickelt wird, wobei die Schrittweite variiert werden kann
    scale = 0.01, mit scale gleich dem Skalierungsfaktor fuer den Plot
===============================================================================
"""
import numpy as np
import quantenmechanik as qm                            # Vorgegebenes Modul
import matplotlib.pyplot as plt
import functools


def potential(x, A):
    """
    Berechnung des Doppelmuldenpotentials fuer gegebene x-Werte. Die x-Werte
    werden als Array uebergeben. Der Parameter A ist ein float und definiert
    die Asymmetrie.
    """
    return x**4 - x**2 - A*x


def gaussian(x, Dx, x_0, heff, p_0):
    """
    Definition des Wellenpaketes, wie es im Aufgabenblatt vorgegeben ist. Dabei
    ist x das Intervall der Schrittweite als Array, x_0 der mittlere Ort als
    float, p_0 der mittlere Impuls als float. Dx ist die Breite des Gausspaket
    als float, heff ist das effektive Planksche Wirkungsquantum als float.
    Zurueckgegeben wird ein Array mit den Werten des Wellenpaketes fuer geg.
    Parameter.
    """
    return 1 / (2*np.pi * Dx ** 2)**(1/4) * np.exp(-(x-x_0) ** 2 /
                                                  (4 * Dx ** 2)) * \
                                                  np.exp(1j/heff * p_0 * x)


def calc_coeff(delta_x, ev, phi):
    """
    Berechnung der Entwicklungskoeffiziente. Die Formel dafuer ist laut der
    Vorlesung definiert als:
    c_n = delta_x * sum(psi^*(x_i)_n*phi_0(x_i)) from i=0 to i=N-1
    Da also die Wellenfunktion psi komplex konjugiert genutzt wird, werden hier
    auch die Numpy-Funktionen np.conjugate und np.transpose genutzt, die genau
    das komplex konjugierte erzeugen. Die uebergebenen Parameter sind delta_x,
    x_0, heff, p_0 und Dx als float. ev und x werden als arrays uebergeben.
    """
    return delta_x*np.dot(np.conjugate(np.transpose(ev)), phi)


def phi_til(c, ew, t, heff, ev):
    """
    Konstruktion des zeitentwickelten Wellenpaketes bei gegebenem t. Die Formel
    dafuer ist laut der Vorlesung definiert als:
    phi(x,t) = sum over n(c_n*exp(-i*E/heff*t)*psi_n(x))
    Die uebergebenen Parameter sind c, ew und ev als arrays und t, heff als
    float.
    """
    return np.dot(ev, c*np.exp(-1j*ew*t/heff))


def expexted_energy_value(c, ew):
    """
    Berechnung des Energieerwartungswertes. Die Formel dafuer ist laut der
    Vorlesung definiert als:
    E_exp=sum over n(abs(c)**2*E_n) wobei E_n die Energieeigenwert.
    Dabei sind c, ew Arrays mit ew als Energieeigenwerte.
    """
    return np.dot(np.abs(c)**2, ew)


def click_event(event, T, ew, ev, delta_x, x, p_0, heff, scale, Dx):
    """
    Funktion fuer Resultat eines Klick-events. Es wird geprueft, ob der Plot
    innerhalb des Plotbereiches mit der linken Maustaste erfolgte. Falls ja,
    wird mit der x-Position des Klicks die Berechnung des Wellenpaketes,
    der Koeffizienten, des zeitentwickelten Wellenpaketes bei t=0, der Norm
    und des Energieerwartungswertes durchgefuehrt. Daraufhin wird das Betrags-
    quadrat des zeitentwickelten Wellenpaketes dynamisch dargestellt.
    Die uebergebenen Parameter sind das Klick-event event, T, ew, ev und x als
    array und delta_x, p_0, heff, scale und Dx als float.
    """
    mode = plt.get_current_fig_manager().toolbar.mode
    if mode == '' and event.inaxes and event.button == 1:
        # Festlegung Startwert
        x_0 = event.xdata
        # Bestimmung des Wellenpaketes mit Anfangswert des Maximums x_0
        phi = gaussian(x, Dx, x_0, heff, p_0)
        # Bestimmung Entwicklungskoeffizienten fuer neuen Startwert
        c = calc_coeff(delta_x, ev, phi)
        # Bestimmung des zeitentwickelten Wellenpaketes fuer t=0
        phi_t = phi_til(c, ew, 0, heff, ev)
        # Berechnung Differenz fuer Norm
        diff = phi - phi_t
        # Berechnung Norm der Differenz unter Ausnutzun der Normdefinition
        norm = np.sqrt(np.dot(diff, diff))
        # Berechnung Energieerwartungswert
        E_exp = expexted_energy_value(c, ew)
        # Geforderte Printausgabe
        print('Es wurde der Startort %f gewaehlt' %x_0)
        print('Fuer gewaehlten Startort ergibt',
              'als Wert fuer die Norm' ,'||phi - phi_t||=', norm.real)
        print('Der Energieerwartungswert entspricht E=%f' %E_exp)
        print('##################')
        # Setzen des Plots von |phi_t(0)|^2 als Parameter
        curves = plt.plot(x, E_exp + scale*np.abs(phi_t)**2, lw=2)
        # Plotanimation des Betragsquadrats des zeitentwickelten Paketes
        for t in T:
            plt.setp(curves[0], ydata=E_exp + scale*
                     (np.abs(phi_til(c, ew, t, heff, ev))**2))
            plt.gcf().canvas.flush_events()
            plt.draw()


def main():
    """
    Hauptfunktion in der Parameter definiert werden. Außerdem werden die
    Werte erstellt, mit denen hauptsächlich gearbeitet wird. Der Plotbereich
    wird hier erstellt und die Eigenfunktionen zu den gehoerenden Eigenwerten
    geplottet. Außerdem wird die Uebergabe in das Klick-event gesteuert.
    """
    L = 1.75                                          # Intervallgrenze [-L,L]
    N = 250                                           # Matrixgroeße
    p_0 = 0.0                                         # mittlerer Impuls
    A = 0.15                                          # Faktor fuer Asymmetrie
    heff = 0.07                                       # eff- Plank. Wirkungsq.
    Dx = 0.1                                          # Breite Wellenpaket
    x_0 = 0.0                                         # mittlerer Ort
    tmax = 12                                        # Gesamtlaufzeit
    tsteps = 1                                        # Animationsschritte
    scale = 0.01                                      # Amplitudenskalierung
    T = np.linspace(0, tmax, 100)             # Zeitenarray
    # Definition diskrete Werte und Schrittweite unter Nutzung des Moduls
    # quantenmechanik.py
    x, delta_x = qm.diskretisierung(-L, L, N, True)
    # Potential als Funktion zur Weitergabe
    V = functools.partial(potential, A=A)
    # Berechnung Eigenwerte und Eigenfunktionen unter Nutzung des Moduls
    # quantenmechanik.py
    ew, ev = qm.diagonalisierung(heff, x, V)

    # Definition des Subplotbereichs, sodass dieser in Funktion uebergeben
    # werden kann
    plt.figure(figsize=(10,8))
    ax = plt.subplot(111)
    # Plot des Potentials und der Eigenfunktionen auf Hoehe der EW unter
    # Nutzung des Moduls quantenmechanik.py
    title = "Zeitentwickeltes gauss'sches Wellenpaket im Doppelmuldenpotential"
    qm.plot_eigenfunktionen(ax, ew, ev, x, V, width=2, betragsquadrat=True,
                            title=title)

    # Klickfunktion als Funktion zur Weitergabe. Uebergeben werden die oben
    # festgelegten Parameter
    click_function = functools.partial(click_event, T=T, ew=ew, ev=ev,
                                       delta_x=delta_x, x=x, p_0=p_0,
                                       heff=heff, scale=scale, Dx=Dx)
    # Verbindung Klick-Funktion und Plotbereich
    plt.connect('button_press_event', click_function)
    plt.show()



if __name__ == '__main__':
    print(__doc__)
    main()

"""
Diskussion zum Programm
zu a)
Beim Start des Wellenpaketes im Minimum sind Bewegungen um das Maximum der
Eigenfunktion beim unteren Eigenwert zu sehen. Vereinzelt treten minimale
Tunneleffekte auf, im Allgemeinen schwankt das Maximum des Wellenpaketes aber
um die Position des Maximums der Eigenfunktion zum betreffenden Eigenwert. Im
Allgemeinen aehnelt das Betragsquadrat des Wellenpaketes beim Start in einem
Minimum des Potentials ueber den gesamten Zeitraum einer Gaussverteilung. Beim
Start des Wellenpaketes im Maximum sind ebenfalls Bewegungen zu sehen, die im
Verlauf der Eigenfunktion zum Eigenwert 0.06 entsprechen. Die Positionen der
einzelnen Maxima des dynamischen Wellenpaketes schwingen um die Maxima der
Eigenfunktion zum eben erwaehnten Eigenwert. Die Schwingungen gehen ueber den
gesamten Potentialbereich. Zeitlich verlaufen die Schwingungen so, dass zu
Beginn das Maximum des Wellenpaketes direkt ueber dem Maximum des Doppel-
muldenpotentials liegt. Kurz darauf verlaufen die Maxima aber Richtung des
Randes und pendeln sich im Bereich der Funktionsmaxima ein.

zu b)
Fuer p_0=0.3 verlaufen die Funktionen wesentlich schneller und schwanken
staerker. Außerdem ist der Energieerwartungswert jetzt sehr viel groeßer als in
Teil a). Im ersten Miminum schwingt das Maximum des Wellenpaketes weiter um den
Bereich des Minimus. Teilweise bilden sich aber auch 2 Maxima aus. Außerdem ist
ein Tunneleffekt sehr viel eher zu sehen. Im zweiten Minimum ist die Schwingung
ebenfalls staerker als in Teil a). Teilweise bilden sich auch zwei Maxima. Die
Schwingung passiert im Allgemeinen aber auch im Bereich des Minimus. Waehlt man
den Bereich des Potentialmaximums als Startwert, befindet sich der Energie-
erwartungswert ungefaehr auf Hoehe des obersten Energieeigenwertes. Die
Schwingung ist sehr dynamisch und die Position der Maxima sowie die Anzahl
schwankt stark, wobei sich die Maxima ueber den gesamten Potentialbereich
verteilen.

zu c)
Fuer ser große Zeiten ist der Tunneleffekt deutlich zu erkennen. Mehrere Male
aendert sich das Minimum, in welchem das Wellenpaket sein Maximum hat. Dabei
ist zu erkennen, dass nach und nach das Maximum des Wellenpaketes zum anderen
Minimum wandert und sich im urspruenglichen Minimum nach und nach abbaut.
Außerdem endet das Wellenpaket nicht in dem Maximum, im den es begann.
Es ist zu beobachten, dass nach T=5590 das Maximum des Wellenpaketes einmal
eine komplette Wanderung durch beide Minima des Potentials vollzogen hat.

zur Bonusaufgabe
Betrachtet man die Aufenthaltswahrscheinlichkeit der Ueberlagerung der
untersten beiden Wellenfunktionen ergibt sich die Formel
|phi(x,t)|^2 = c1^2|psi1|^2 + c2^2|psi2|^2 + c1c2psi1*psi2 exp(it/heff(E2-E1))
                                           + c1c2psi1psi2* exp(it/heff(E1-E2)
             = c1^2|psi1|^2 + c2^2|psi2|^2 + 2i sin(t/heff(E2-E1))

Nun wird der Wert fuer T gesucht, bei dem gilt
|phi(x,0)|^2 = |phi(x,T)|^2
Hier gilt
           0 = sin(T/heff(E2-E1))
Dies ist erfuellt fuer
           T = 2npi/2 * heff * (E2-E1)^(-1)
Mit n aus den natuerlichen Zahlen. Fuer n=1 folgt fuer T mit den von meinem
Programm berechneten Werten T~5622.
Es zeigt sich also, dass diese Naeherung mit der Ueberlagerung der untersten
beiden Wellenfunktionen zwar ein abweichendes Ergebnis liefert, es aber schon
eine relativ genaue Naeherung ist. Vor allem in Anbetracht dessen, dass auch
obige T=5590 nur eine grobe Ablesung war, die meiner Schaetzung entsprach und
der tatsaechliche numerische Wert leicht davon abweichen kann.
"""
