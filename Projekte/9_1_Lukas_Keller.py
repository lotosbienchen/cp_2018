#!usr/bin/python
"""
Programm zum neunten Uebungsblatt 'Diffusion mit Drift und Absorption'.
Es wird unter Verwendung der Langevin-Gleichung die gerichtete Diffusion von
Teilchen dargestellt, wobei wir einen absorbierenden Rand betrachten. Es wird
dabei der Fall mit Absorption mit dem theoretisch zu erwartenden Fall verglich-
en, wenn keine Absorption vorlaege.
===============================================================================
Mit einem Linksklick in den Plotbereich kann die dynamische Zeitentwicklung
gestartet werden. Es sind dann die Entwicklungen von 4 Plots zu sehen. Der
Plot oben links zeigt die Wahrscheinlichkeitsdichteverteilung in Form eines
Histogramms(blau), den theoretischen Fall mit Absorption(orange) und den
theoretischen Fall ohne Absorption(schwarz). Die drei anderen Plots zeigen
gemaeß ihres Titels die jeweilige Groeße, wobei schwarz den theoretischen Wert
ohne und orange den theoretischen Wert mit Absorption anzeigt.
===============================================================================
Die Simulation laeuft mit den folgenden Parametern:
    x_0 = 0, wobei x_0 gleich dem Startwert des Teilchens
    v_drift = 0.1, wobei v_drift gleich der Driftgeschwindigkeit der Teilchen
    D = 1.5, wobei D gleich der Diffusionskonstante
    T_max = 40, wobei T_max gleich dem maximal zu betrachtenden Zeitwert
    dt = 0.01, wobei dt gleich der Zeitschrittweite
    tsteps = 100, wobei tsteps dazu dient, dt im Array zu realisieren
    x_abs = 15, wobei x_abs gleich dem Ort des absorbierenden Randes
    R = 10000, wobei R gleich der Anzahl der Realisierungen
    bins = 100, wobei bins das Array fuer die bins fuer das Histogramm und
                bins = 100 die Laenge des Arrays
"""
import functools
import numpy as np
import matplotlib.pyplot as plt


def normal_dsb(x, mu, v):
    """
    Funktion zur Berechnung der Normalverteilung, wie sie in der Vorlesung
    definiert wurde. Uebergeben werden x als Array und mu und v als float
    """
    return 1/np.sqrt(2*np.pi*v)*np.exp(-(x-mu)**2/(2*v))


def langerin(x_t, v, dt, D, R):
    """Funktion welche über die in der Vorlesung definierte Langevin-Gleichung
    aus dem aktuellen Ort x_t, der Driftgeschwindigkeit v und der
    Diffusionskonstanten D für R Realisierungen die neuen Postionen nach einer
    Zeit dt berechnet. Dafuer wird x_t als Array und v,dt, D und R als float
    uebergeben.
    """
    # Berechnung normal verteilter Parameter fuer weißes Rauschen
    nt = np.random.randn(R)

    # Berechnung der Langevin-Funktion
    return x_t + v*dt + np.sqrt(2*D*dt)*nt


def prob(x, x_0, v, t, D, x_abs):
    """
    Funktion zur Berechnung der Wahrscheinlichkeitsdichteverteilung, wenn ein
    absorb. Rand vorliegt, wie sie in der Vorlesung definiert wurde. Dabei wird
    x als Array und x_0, v, t, D, x_abs als float
    """
    return normal_dsb(x, x_0 + v*t, 2*D*t) - \
           normal_dsb(x, 2*x_abs - x_0 + v*t, 2*D*t) * \
           normal_dsb(x_abs, x_0 + v*t, 2*D*t) / \
           normal_dsb(x_abs, 2*x_abs - x_0 + v*t, 2*D*t)


def time_magic(event, x_0, v, dt, D, R, T_max, tsteps,
               x_abs, ax1, ax2, ax3, ax4, bins):
    """
    Funktion fuer die dynamische Darstellung der Zeitabhaengigkeit der zu
    berechnenden Groeßen. Wenn mit der linken Maustaste in den Plotbereich
    geklickt wird, wird ueber das gesamte Zeitenarray iteriert und die
    Wahrscheinlichkeitsdichte, die Norm, die Varianz und der Erwartungswert
    zeitlich dynamisch dargestellt, wobei dafuer oben benutzte Funktionen
    genutzt werden. Fuer ganzzahlige Zeiten wird dann der Plot aktualisiert.
    Die Plots werden auch mit den theoretisch zu erwartenden Plots verglichen,
    wenn keine Absorption vorläge. Uebergeben werden das click-event, und
    x_0, v, dt, D, R, T_max, tsteps, x_abs als float und die in der main def.
    Subplots ax1-ax4, sowie bins als Array
    """
    mode = plt.get_current_fig_manager().toolbar.mode
    if mode == '' and event.inaxes and event.button == 1:
        # Definition x-Array, wobei alle Eintraege gleichen Startwert haben
        x = np.ones(R) * x_0
        # Definition Zeitarray bis T_max und dt = 0.01
        T = np.linspace(0, T_max, T_max*tsteps + 1)

        # Definition x_exp-Array, welches spaeter Werte fuer theoretische
        # Entwicklung enthaelt
        x_exp = x

        # Definition Arrays fuer Berechnung der gesuchten Groeßen. Die Arrays
        # zur Berechnung der Wahrscheinlichkeit, werden mit dem Wert t=1
        # initialisiert.
        p = prob(x, x_0, v, 1, D, x_abs)              # Wahrsch. m. ab. Rand
        p_exp = normal_dsb(x_exp, x_0 + v, 2*D)       # Wahrsch. o. ab. Rand
        norm = np.nan*np.zeros(len(T))                # Normarray
        var = np.nan*np.zeros(len(T))                 # Varianzarray
        mean = np.nan*np.zeros(len(T))                # Erwartungswertarray

        # Berechnung Startwerte fuer Norm-, Varianz- und Erwartungswertarray,
        # da fuer t=0 keine Berechnung gestartet werden kann, weil sonst durch
        # 0 geteilt wuerde.
        norm[0] = len(x_exp)/R                        # Startwert Norm
        var[0] = np.var(x)                            # Startwert Varianz
        mean[0] = np.mean(x)                          # Startwert Erwartungsw.

        # Definition Wahrscheinlichkeitsdichteplot
        prob_plot = ax1.plot(x, p, c='orange', lw=2)
        # Definition Wahrscheinlichkeitsdichteplot fuer Fall ohne Absorption
        expprob_plot = ax1.plot(x, p_exp, c='black', lw=2)
        # Definition Normplot
        norm_plot = ax2.plot(T, norm, c='orange', lw=2)
        # Definition Normplot fuer Fall ohne Absorption
        expnorm_plot = ax2.plot(T, len(x_exp)/R*np.ones(len(T)),
                                c='black', lw=2)
        # Festlegung der Achsen, da sonst Achsenlimitierung zu klein definiert
        # wird.
        ax2.set_ylim([0.5, 1.5])
        # Definition Varianzplot
        var_plot = ax3.plot(T, var, c='orange', lw=2)
        # Definition Varianzplot fuer Fall ohne Absorption
        expvar_plot = ax3.plot(T, 2*D*T, c='black', lw=2)
        # Definition Erwartungswertplot
        mean_plot = ax4.plot(T, mean, c='orange', lw=2)
        # Definition Erwartungswertplot fuer Fall ohne Absorption
        expmean_plot = ax4.plot(T, x_0 + v*T, c='black', lw=2)

        # Zeitentwicklung
        for i in range(len(T)):
            # Berechnung des Ortes nach T[i] Zeiteinheiten
            x = langerin(x, v, dt, D, len(x))
            # Berechnung des Ortes fuer Fall ohne Absorption
            x_exp = langerin(x_exp, v, dt, D, len(x_exp))
            # Herausfiltern der absorbierten Teilchen, wenn x>= x_abs
            x = x[x<x_abs]
            # Definition 2er x-Arrays fuer den Plot der Wahrscheinlichkeits-
            # verteilungen. Dabei ist x_prob das Array fuer den Fall mit und
            # x_expplot das Array fuer den Fall ohne Absorption
            x_prob = np.linspace(np.min(x), np.max(x), len(x))
            x_expplot = np.linspace(np.min(x_exp), np.max(x_exp), len(x_exp))
            # Aktualisierung der Norm-, Varianz- und Erwartungswertarrays fuer
            # das jeweilige T[i]
            norm[i] = len(x)/R
            var[i] = np.var(x)
            mean[i] = np.mean(x)
            # Berechnung der Gewichte fuer die Bins
            weights=np.ones(len(x))/(R*(bins[1] - bins[0]))

            # Untersuche den Fall ganzzahliger T[i], die groeßer sind als 0
            if T[i] in T[::tsteps] and T[i] > 0:
                # Bereinigen der Subplots
                ax1.patches = []
                # Setze neue Werte fuer den Wahrscheinlichkeitsplot im Fall
                # mit Absorption. Beachte dabei das reduzierte x-Array
                plt.setp(prob_plot[0], xdata=x_prob,
                                       ydata=prob(x_prob, x_0, v,
                                                  T[i], D, x_abs))
                # Setze neue Werte fuer den Wahrscheinlichkeitsplot im Fall
                # ohne Absorption. Es wird das theoretische x-Array genutzt.
                plt.setp(expprob_plot[0], xdata=x_expplot,
                                          ydata=normal_dsb(x_expplot, x_0
                                                           + v*T[i], 2*D*T[i]))
                # Setze neue Werte fuer den Norm-, Varianz- und Erwartungswert-
                # plot. Hier ist gezeigt, wie die Werte zum Zeitpunkt T[i]
                # aussehen. Es wird deswegen ueber das Zeitenarray geplottet.
                plt.setp(var_plot[0], xdata=T, ydata=var)
                plt.setp(norm_plot[0], xdata=T, ydata=norm)
                plt.setp(mean_plot[0], xdata=T, ydata=mean)

                # Plotten des Hisogramms
                ax1.hist(x, bins=bins, weights=weights, color='b')

                # Plotaktualisierung
                plt.gcf().canvas.flush_events()
                plt.draw()



def main():
    """
    Hauptfunktion, in der die genutzten Parameter und eine Plotumgebung
    definiert werden und die Funktion zum Starten der Simulation an
    Klick-Events gebunden wird.
    """
    # Parameter
    x_0 = 0
    v_drift = 0.1
    D = 1.5
    T_max = 40
    dt = 0.01
    tsteps = 100
    x_abs = 15
    R = 10000
    bins = np.linspace(-40, x_abs, 100)

    # Plotumgebung mit 4 Subplots, Grobeinstellungen definieren
    plt.figure("Diffusion mit Drift und Absorption bei x = %i" %x_abs,
               figsize=(8,6))
    plt.suptitle("Diffusion mit Drift und Absorption bei x = %i" %x_abs)
    # Wahrscheinlichkeitsplot
    ax1 = plt.subplot(221)
    ax1.set_xlabel('x')
    ax1.set_ylabel(r'$P(x)$ pro Binbreite')
    # Normplot
    ax2 = plt.subplot(222)
    ax2.set_xlabel('t')
    ax2.set_ylabel(r'Norm $R(t)/R$')
    # Varianzplot
    ax3 = plt.subplot(223)
    ax3.set_xlabel('t')
    ax3.set_ylabel(r'Varianz')
    # Erwartungswertplot
    ax4 = plt.subplot(224)
    ax4.set_xlabel('t')
    ax4.set_ylabel('Erwartungswert')

    # Time Magic Funktion startet bei Mausklick die Simulation/Visualisierung
    on_click = functools.partial(time_magic,x_0=x_0, v=v_drift, dt=dt, D=D,
                                            R=R, T_max=T_max, tsteps=tsteps,
                                            x_abs=x_abs, ax1=ax1, ax2=ax2,
                                            ax3=ax3, ax4=ax4, bins=bins)
    plt.connect('button_press_event', on_click)

    plt.show()

if __name__ == "__main__":
    print(__doc__)
    main()

"""
Diskussion der Fragen

zu a) Durch den absorbierenden Rand werden die gesuchten Groeßen immer kleiner
und weichen von dem theoretisch zu erwartenden Wert immer mehr ab. Dies laesst
sich dadurch erklaeren, dass die x-Verteilung zum einen ab einem bestimmten
Zeitpunkt schmaler wird, als die theoretische Normalverteilung und sich zum
anderen ihr Maximum nach links im Vergleich zur theoretischen Normalverteilung
verschiebt. Deutlich wird diese Abweichung ab ungefaehr 10 Zeiteinheiten.
Nach 10 Zeiteinheiten wurde also eine relevante Anzahl Teilchen absorbiert.

zu b) Fuer den Fall dt=1 ist am deutlichsten zu erkennen, dass das Maximum des
Erwartungswertes und somit der Peak einen groeßeren x-Wert hat und naeher
am Maximum des theoretischen Erwartunswertes ohne Absorption ist. Dies ist
auch daran zu erkennen, dass der Erwartungswert langsamer abfaellt.

zu c) Setzt man v=0.5 ist die Verteilung stark gestreckt. Das Maximum ist
sehr viel geringer als im Fall v=0.1 und im Vergleich zur theoretischen
Verteilung fuer den Fall ohne Absorption ist klar zu erkennen, dass die
Teilchen eigentlich sehr viel weiter kommen sollten, als es der Rand zulaesst.
Ab T[i] = 10 veraendern sich zudem Erwartungswert und Varianz kaum noch und
verlaufen großteils konstant. Der absorbierende Rand filtert fuer diesen Fall
also den Großteil der Teilchen aus dem betrachteten System und die
verbleibenden sind zum Ende hin annaehernd gleichverteilt.
"""
