#/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import functools

x_0 = 0
y_0 = 0
ampl = 0.8

def create_loop(x_0, y_0):
    t = np.linspace(0.0, 5.0, 1000)
    print(t)
    r = (1/2)**t
    phi = 2*np.pi*t
    print(phi)
    x = x_0 + r*np.sin(phi)
    y = y_0 + r*np.cos(phi)
    return x,y

def click_event(event, amplitude):
    xpos = event.xdata
    ypos = event.ydata

    x,y = create_loop(xpos, ypos)
    plt.plot(x,y)
    plt.draw()
    print('here comes the function')

print(create_loop(x_0, y_0))

def create_plot(x_0, y_0, ampl):
    x,y = create_loop(x_0, y_0)
    plt.plot(x,y)

    click_function = functools.partial(click_event, amplitude=ampl)
    plt.connect('button_press_event', click_function)
    plt.show()

create_plot(x_0,y_0, ampl)
