#!usr/bin/python
'''
Programm für das erste Aufgabenblatt "Standardabbildung" im Sommersemester
2018. Es wird die Dynamik eines gekickten Rotors simuliert, was in einer
Darstellung chaotischer Phasenraeume resultiert.
===
Die Startwerte werden durch einen Linksklick mit der Maus im Plotbereich
festgelegt.
'''

import functools                          # Modul zur Interaktionen der Maus
import numpy as np                        # Modul fuer allgemeine Berechnungen
import matplotlib.pyplot as plt           # Modul fuer Plots
                                          # mit dem Plot

def create_orb_point(theta, impulse, sok):
    '''
    Erstellt für uebergebene Werte fuer Theta und den Impuls bei festgesetzter
    Kickstaerke die darauf folgenden Punkte.
    '''
    # Die folgenden Theta- und Impulswerte werden mithilfe der Formeln fuer
    # die Standardabbildung berechnet und an die periodischen Randbedingungen
    # mit der modulo-Funktion angepasst.
    theta_next = (theta + impulse) % (2*np.pi)
    p_next = (impulse + sok*np.sin(theta_next) + np.pi) % (2*np.pi) - np.pi
    return theta_next, p_next

def create_orb(t_0, p_0, sok, N):
    '''
    Es wird fuer eine gegebene Startposition (siehe click_event) ueber alle zu
    berechnenden Punkte iteriert.
    '''

    # Erstellung der Arrays fuer Plotkoordinaten in Iterationsgroeße
    x_var = np.zeros(N + 1)
    y_var = np.zeros(N + 1)

    # Festlegung Startwerte als erste Array-Werte
    x_var[0], y_var[0] = t_0, p_0

    # Iteration ueber alle verbleibenden Nullwerte des Arrays
    for i in range(1, N + 1):
        x_var[i], y_var[i] = create_orb_point(x_var[i-1], y_var[i-1], sok)

    return x_var, y_var


def click_event(event, sok, N):
    '''
    Festlegung eines Startpunktes mittels linker Maustaste und Erstellung des
    Plots für gewaehlten Startwert
    '''
    # Definition des gewuenschten Klickevents, bei dem eine Aktion ausgeloest
    # wird. Eine Reaktion kommt nur, wenn die linke Maustaste genutzt wird,
    # es wird kein neuer Orbit gestartet, wenn ein 'Ziehevent' registriert
    # wird.
    mode = plt.get_current_fig_manager().toolbar.mode
    if mode == '' and event.inaxes and event.button == 1:
        # Festlegung Startwerte
        x_0 = event.xdata
        y_0 = event.ydata

        print('Es wurde der folgende Startpunkt gewaehlt:', (x_0, y_0))
        print('===')
        print('Waehle einen neuen Startpunkt')

        # Uebergabe Startwerte für Iteration
        x_var, y_var = create_orb(x_0, y_0, sok, N)

        # Erstellung Plot für gewaehlte Startwerte
        plt.plot(x_var, y_var, ls='', marker='.', ms=1, mew=0.7)
        plt.draw()


def create_plot():
    '''
    Hauptfunktion in der die veraenderlichen Variablen variiert werden koennen
    und der Plotbereich nach den Vorgaben erstellt wird.
    '''

    print(__doc__)

    strength_of_kick = 1.05
    N = 5000
    # Bei einem Mausklick im Plot wird mit der click_function im click_event
    # das Event ueberprüft und weiterbearbeitet. Es wird hier noch die
    # Varaible K in click_event uebergeben, die die Kickstaerke festlegt
    click_function = functools.partial(click_event, sok=strength_of_kick, N=N)
    plt.connect('button_press_event', click_function)

    plt.rc('text', usetex=True)                             # Labeltext in
    plt.rc('font', family='serif')                          # LaTex

    plt.subplot(111, aspect=1.0)                          # Plotbereich
    plt.title('Standarbabbildung gekickter Rotor')
    plt.axis([0, 2*np.pi, -np.pi, np.pi])                   # Axenbereich
    plt.xlabel(r'\textbf{Winkel} \theta')                   # Labelung Achsen
    plt.ylabel(r'\textbf{Impuls} p')

    plt.show()                                              # Plotausgabe

if __name__ == '__main__':
    create_plot()

'''
Beschreibung der Beobachtung bei verschiedenen Werten für K:

Die Veraenderung des Parameters K beeinflusst den sichtbaren Plot stark. Im
Folgenden moechte ich das Bild für mehrere K-Werte analysieren:

K = 0.0: In der Darstellung sind immer Graphen konstant in p zu sehen, da hier
der theta-Teil in p verschwindet.
Fuer K=0.0 liegt also der ungekickte Rotor vor.

K = 0.1: Fuer Thetastartwerte nahe 0 ist noch eine lineare Dynamik erkennbar,
die auch bei einem harmonischen Oszillator zu erwarten waere, für groeßere
Thetawerte werden die Graphen wieder nahezu konstant

K = 1.0: Der Bereich mit linearer Dynamik wird sehr viel kleiner und am Rand
des groeßten Bereiches linearer Dynamik sind mehrere kleinere Zentren mit
linearen Bewegungen zu finden. Die chaotischen Bereiche werden das erste mal
stark sichtbar und sind um das Zentrum verteilt. Darin sind teilweise wieder
kleinere Bereiche mit linearer Dynamik

K = 2.6: Der zentrale Bereich linearer Dynamik ist im Bereich zum vorherigen
Wert fuer K stark reduziert und ist flaechendeckend von Bereichen
nichtlinearer Dynamik umschlossen. Vereinzelt sind darum herum noch 4 kleinere
Bereiche, in denen lineare Dynamik erkennbar ist

K = 6.0: Der ganze Plotbereich ist von nichtlinearer Dynamik gekennzeichnet,
es gibt keine klaren Phasenraumlinien. Es gibt an der oberen und unteren
Plotgrenze noch 2 kleine Zentren in denen lineare Dynamik erkennbar ist

K = 6.5: Aehnlich zum vorherigen ist der ganze Plotbereich wieder von nicht-
linearer Dynamik aufgefuellt. Mittig im Plot sind zwei Bereiche mit linearer
Dynamik erkennbar

Bei den Plots für die Werte von K = 1.0 - K = 6.5 scheinen sich beim Zoomen
auf die kleineren Bereiche mit linearer Dynamik die Darstellungen zu
wiederholen, auch hier sind um die klaren Plotlinien kleinere Bereiche mit
scheinbar abgetrennter Dynamik, die lokal von chaotischen Bereichen umgeben
sind. Die vergroeßerten Bereiche aehneln bei den Werten für K > 1.0 wieder der
zu sehenden Darstellung bei K = 1.0.
'''
