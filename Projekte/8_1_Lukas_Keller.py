#!usr/bin/python3
"""
Programm fuer das neunte Uebungsblatt "Ideales Gas: Druckmessung". Es wird
fuer N Teilchen die Zahl der Reflexion gezaehlt, die an der rechten Seite
eines Quaders geschehen. Diese Teilchen haben dabei einen zufaelligen Startort
und eine zufaellige Geschwindigkeit, die sie pro Zeitintervall zuruecklegen
koennen. Damit wird der Druck berechnet und die Wahrscheinlichkeitsverteilung
des Druckes als Histogramm dargestellt. Mithilfe der Standardabweichung und
dem Erwartungswert des Druckes wird außerdem die normierte Gauss-Verteilung
dargestellt, welche um die Bin-Breite skaliert ist.
===============================================================================
Die dabei verwendeten Werte sind:
    N = 8, mit N der Anzahl der Teilchen
    dt = 3, mit dt gleich der Anzahl der Zeitintervalle
    R = 10000, mit R gleich den Realisierungen mit jeweils N Teilchen
    v_spread = 1, mit v_spread gleich der Streuweite von v
    bins = 70, mit bins gleich der Anzahl der genutzten Bins
"""
import numpy as np
import matplotlib.pyplot as plt

def get_hit(x, v, dt):
    """
    Funktion zur Berechnung der Reflexionen an der rechten Wand. Es wird dabei
    gezaehlt, wie oft das Teilchen mit einer zufaelligen Geschwindigkeit und
    einem zufaelligen Startort bezogen auf das Zeitintervall reflektiert wird.
    Analytisch berechnet sich das durch
    n = |X|/2 + 1/2
    wobei n die Reflexionsanzahl und X der Ort nach dem Zeitintervall dt.
    Uebergeben werden x,v als array und dt als float.
    """
    # Berechnung des Ortes nach dt Zeitintervallen
    X = x + v*dt

    rn = np.abs(X)/2 + 1/2
    return np.int32(rn)

def pressure(N, dt, v, rn):
    """
    Funktion zur Berechnung des Druckes auf die rechte Seitenflaeche des
    Quaders. Es wird die Formel aus der Vorlesung genutzt, die auf
    einheitenlose Groeßen reduziert wurde. Uebergeben werden N, dt als float u.
    v, rn als Arrays
    """
    p_A = 2/(N*dt)*np.sum(rn * np.abs(v), 1)
    return p_A

def gaussian_distribution(sigma, x, mu):
    """
    Funktion zur Berechnung der Gauss-Verteilung, wie sie bei Wikipedia def.
    wird. Die Formel ist definiert als
    F(x) = (2pi*sigma^2)^(-1/2)*exp(-(x-mu)^2/(2sigma^2))
    Uebergeben wird sigma, mu als float und x als array
    """
    gd = 1/(np.sqrt(2*np.pi*sigma**2))*np.exp(-(x-mu)**2/(2*sigma**2))
    return gd


def main():
    """
    Hauptfunktion in der die Parameter festgelegt und die genutzten
    Arrays erstellt werden. Außerdem wird die Uebergabe in weitere Funktionen
    gesteuert und die Standardabweichung sowie der Erwartungswert fuer die
    Gauss-Verteilung berechnet. Schlussendlich wird der Plot fuer das
    Histogramm und die Gauss-Verteilung erstellt.
    """
    N = 8                                            # Teilchenanzahl
    dt = 3                                           # Zeitintervalle
    R = 10000                                        # Realisierung
    v_spread = 1                                     # Streuweite
    bins = 70                                        # Anzahl der Bins

    # Erstellung zufaelliger Werte fuer den Startort und die Geschwindigkeit
    # der Teilchen. Dabei folgen die Zufallszahlen im Ortarray einer
    # Gleichverteilung mit Zufallszahlen zwischen 0,1. Die Zufallszahlen im
    # Geschwindigkeitsarray folgen einer Normalverteilung mit Zufallszahlen
    # zwischen 0 und v_spread. Beide Arrays sind 2-dimensional.
    x = np.random.random((R, N))                     # Ortarray
    v = np.random.normal(0, v_spread, (R, N))        # Geschwindigkeitsarray


    # Berechnung der Zahl der Reflexionen rn
    rn = get_hit(x, v, dt)

    # Berechnung des Druckes an der rechten Seite
    p_A = pressure(N, dt, v, rn)

    # Erstellung des Definitionsbereiches fuer die Berechnung der Normalver-
    # teilung. Dabei wird mit dem Minimum und dem Maximum ein p_A ein
    # Definitionsbereich erstellt.
    x_gd = np.linspace(min(p_A), max(p_A), R)

    # Berechnung Erwartungswert
    p_mid = np.mean(p_A)
    # Berechnung Standardabweichung
    p_std = np.std(p_A, ddof=1)
    # geforderter Plot
    print("Der Erwartungswert des Druckes auf die rechte Seite betraegt %0.3f"\
          %p_mid)
    print("Die Standardabweichung des Druckes betraegt %0.3f" %p_std)

    # Berechnung Normalverteilung
    gd = gaussian_distribution(p_std, x_gd, p_mid)

    # Allgemeine Plotbereicheinstellungen
    plt.figure(figsize=(10, 8))
    plt.subplot(111)
    plt.title("Druck durch {} Teilchen auf die rechte Wand eines Quaders"\
              .format(N))
    plt.xlabel(r"$p_A$")
    plt.ylabel(r"$P(p_A)$ pro Bin oder $P(p_A)$ pro Druckbereich")

    # Histogrammplot mit 100 Bins und weight-Funktion zur Skalierung des
    # Histogramms auf die genaue Binbreite. Dadurch kann genau die Wahrschein-
    # lichkeit fuer jeden Bin abgelesen werden. Das Histogramm wird in die
    # Variable hist gespeichert, um die bin-Breite berechnen zu koennen. Hier-
    # fuer ausgenutzt, dass plt.hist unter anderem die bins-Stuetzstellen
    # zurueckgibt.
    hist = plt.hist(p_A, bins=bins, weights=np.ones(R)/R,
             label="Histogrammplot ueber\n R=%i Realisierungen")

    # Berechnung der Binweite
    bin_width = hist[1][1] - hist[1][0]
    print('Die Binbreite/ Skalierung der Normalverteilung ist %0.3f'
          %bin_width)
    # Normalverteilungsplot fuer berechneten Mittelwert und Standardabweichung.
    # Außerdem wird die Normalverteilung mit der oben berechneten Binweite
    # skaliert. Dies wird gemacht, damit man die Wahrscheinlichkeiten der Bins
    # gut ablesen kann und trotzdem noch den Histogrammverlauf mit der Normal-
    # verteilung vergleichen kann. Da fuer die Normalverteilung laut Aufgabe-
    # stellung nur der Verlauf wichtig ist, wurde sich hier fuer die Skalierung
    # der Normalverteilung entschieden.
    plt.plot(x_gd, bin_width*gd, lw=2, color='orange',
             label="Normalverteilung mit\n $\mu$={0:0.3f}, $\sigma$={1:0.3f}"\
             .format(p_mid, p_std))
    # Erstellung Legende an freier Stelle
    plt.legend(loc=0)
    plt.show()



if __name__ == '__main__':
    print(__doc__)
    main()

"""
Diskussion der Fragen

a) Fuer N = 80 passt sich der Histogrammplot stark dem Verlauf der Normal-
verteilung an. Außerdem pendelt sich der Mittelwert langsam bei <p_A> ~ 1 ein.
Somit verschiebt sich das Maximum des Histogramms in Richtung p_A = 1. Dies
zieht eine Verrueckung des Histogramms nach rechts mit sich und der bei N = 8
stark ausgepraegte rechte Auslaeufer wird kuerzer, so dass die Symmetrie des
Histogramms auch eher der Symmetrie der Normalverteilung entspricht.
Man kann auch erkennen, dass der Definitionsbereich im Vergleich zu N = 8
sehr viel enger wird. Dies drueckt sich auch in einer Verminderung der
Standardabweichung aus, die mit Erhoehung von N stark sinkt. Damit steigt auch
die Wahrscheinlichkeit, einen Druck von p_A = 1 zu erhalten.
Erhoeht man N noch weiter, sieht man, dass dieser Prozess immer mehr zunimmt.
Dies ist vor allem fuer die Aufgabe b) eine interessante Betrachtung

b) Erhoeht man N immer weiter, ist <p_A> ab einem gewissen Punkt nahezu 1.0 und
die Standardabweichung beweigt sich in einem Bereich von ~ 0.01. Ab einem Wert
von N ~ 25000 ist die numerische Auswertung auf Grund eines Memory Errors in
der random.normal Funktion nicht mehr moeglich. Geht man aber theoretisch
weiter bis N = 6*10e23, legt die vorherige Betrachtung die Vermutung nahe, dass
das Histogramm vollstaendig gegen den Verlauf der Normalverteilung strebt und
man durch genuegend kleine Bins das Histogramm dem Verlauf der Normalverteilung
komplett annaehern kann. Durch die starke Verminderung der Standardabweichung
bis N = 25000, laesst sich vermuten, dass diese fuer sehr große N stark sinkt.
Damit wuerde der relevante Definitionsbereich immer kleiner werden und sich
irgendwann einer Delta-Funktion um p_A = 1 annaehern.
"""
