#!usr/bin/python3
'''
Programm fuer das fuenfte Aufgabenblatt 'Quantenmechanik von 1D-Potentialen I'.
Es werden fuer ein Teilchen im asymmetrischen Doppelmuldenpotential
V(x) = x^4 - x^2 - Ax die zugehörigen Eigenfunktionen und Eigenenergien
bestimmt, die kleiner als 0.25 sind.
Die verwendeten Parameter sind A = 0.15 und h_eff = 0.07. Außerdem sei
x in [-1.7, 1.7] und es werden 250 Iterationen berechnet. Die Eigenfunktionen
werden zudem skaliert dargestellt. Dadurch sind die Amplituden zwar reduziert,
allerdings ist der Funktionsverlauf besser zu erkennen. Der Skalierungsfaktor
ist scale = 0.015
'''
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eigh

def potential(x, A):
    """
    Diese Funktion berechnet das Doppelmuldenpotential fuer die gegebenen x-
    Werte. Die uebergebenen Werte sind dabei ein x-Array und A als float, wobei
    der Parameter A das Potential so beeinflusst, dass ein asymetrisches
    Potential bei A != 0 entsteht.
    """
    return x**4 - x**2 - A*x


def get_disvals(N, xmax, xmin):
    """
    Hier werden die diskreten Werte fuer vorher festgelegte Grenzen und
    vorher festgelegte Matrixgroeße ermittelt. Hier sind alle uebergebenen
    Parameter floats.
    """
    # Berechnung der Diskretisierungsschrittweite nach der Formel aus der
    # Vorlesung
    delta_x = (xmax - xmin)/(N + 1)
    # Berechnung der diskreten Werte unter Nutzung der
    # Diskretisierungsschrittweite
    x = np.linspace(xmin + delta_x, xmax - delta_x, N)
    return x, delta_x



def get_eigen(x, delta_x, N, heff, V):
    """
    Berechnung der Eigenwerte und -funktionen anhand der vorher berechneten
    diskreten Werte. Dafuer wird die Hauptdiagonale der Matrix gemaeß der Ver-
    fahrensweise in der Vorlesung mit V(x) + 2*z gefuellt und die darueber und
    darunter liegenden Diagonalen mit -z. Zum Schluss werden die Eigenvektoren
    entsprechend normiert. Die uebergebenen Parameter sind hierbei x als Array,
    delta_x als float, N und heff als float, V als Funktion und norm wieder als
    float.
    """

    # Berechnung des z-Parameters nach Vorlesung
    z = heff**2 / (2.0*(delta_x)**2)
    # Fuellen der Hauptdiagonale der Matrix gemaeß Vorlesung und Hinweis auf
    # Aufgabenblatt. Die Werte der Matrix abseits der Hauptdiagonalen sind 0
    matrix = np.diag(V + 2.0*z)
    # Erhalten der Indizes in der Matrix
    i, j = np.indices(matrix.shape)
    # Füllen der Diagonalen direkt ueber und unter der Hauptdiagonalen (HD) mit
    # -z gemaeß der Vorlesung
    matrix[i==j-1] = -z                            # Diagonale unter HD
    matrix[i==j+1] = -z                            # Diagonale ueber HD

    # Nutzen der scipy-Funktion zum Erhalt der Eigenwerte und Eigenfunktionen
    ew, ev = eigh(matrix)

    # Normieren der Eigenwerte mit vorher ermittelter Norm. Eigentlich ist es
    # eher eine Neunormierung, da die eigh-Funktion die Eigenfunktionen intern
    # anders normiert.
    ev = ev/np.sqrt(delta_x)

    return ew, ev


def get_plot(x, V, Emax, ew, ev, scale):
    """
    Funktion zur Erstellung des Plots. Es wird hier das Potential geplottet,
    alle Energieeigenwerte, die kleiner als der gewuenschte Schwellenwert sind,
    und die dazu gehoerigen Eigenfunktionen. Uebergeben werden x als Array, V
    als Funktion, Emax und scale als floats und ew, ev als Arrays. Außerdem
    ein Array mit zu plottenden Farben
    """
    #Plotten des Potentials
    plt.plot(x, V, c='black', lw=2)

    # Farbwerte fuer die Eigenwerte und -funktionen
    colors = np.array(['blue', 'green', 'red', 'yellow', 'magenta',
                       'cyan', 'orange', 'grey'])

    # Zahl der EW kleiner gleich Emax
    length = len(ew[ew <= Emax])

    # Da die range-Funktion hier eigentlich nur bis length-1 geht, erhaelt man
    # durch length den letzten Eigenwert kleiner gleich Emax
    for i in range(length):
        # Plotten der Eigenwerte. Die Zeros werden dazu addiert, damit ein
        # Array entsteht, welches konstant den Wert ew[i] hat
        plt.plot(x, ew[i] + np.zeros(len(x)), alpha=0.5, c=colors[i%8],
                 ls='--', lw=2)
        # Plotten der Eigenfunktionen auf Hoehe der Eigenwerte. Der
        # Skalierungsfaktor reduziert die Amplitude und ermoeglicht daher eine
        # bessere Darstellung.
        plt.plot(x, ew[i] + scale*ev[:, i], c=colors[i%8], lw=2)
        # Labeltext fuer EW
        str = ("EW=%.3f" %(ew[i]))
        # Label fuer EW
        plt.text(x[1], ew[i]+0.005, str)



def main():
    """
    Hauptfunktion, in der die Parameter definiert werden und an die geforderten
    Unterroutinen uebergeben werden. Sie steuert zudem die Abfolge und das
    Weitergeben der Werte aus den Unterroutinen. Außerdem wird hier der
    Plotbereich definiert, in den dann die Graphen aus der Unterroutine
    eingefuegt werden.
    """
    L = 1.7                                       # Intervallgrenzen mit [-L,L]
    N = 250                                       # Matrixgroeße
    heff = 0.07                                   # Groeße des Vorfaktors
    A = 0.15                                      # Parameter fuer Asymetrie
    Emax = 0.25                                   # Energiegrenze fuer
                                                  # Darstellung
    scale = 0.015                                 # Skalierungswert fuer
                                                  # Darstellung
    x, delta_x = get_disvals(N, L, -L)            # diskrete Werte und Diskret-
                                                  # isierungsschreitweite
    V = potential(x, A)                           # Potential des Oszillators

    # Abspeicherung der Eigenwerte und Eigenfunktionen in Variablen
    ew, ev = get_eigen(x, delta_x, N, heff, V)

    # Plotbereicheinstellungen
    plt.figure(figsize=(10,8))
    plt.subplot(111)
    plt.title('Teilchen im asym. Doppelmuldenpotential')
    plt.axis([np.min(x), np.max(x), np.min(V), Emax])
    plt.xlabel('x')
    plt.ylabel('Potential V(x) u. EF zum Wert EW')
    # Zu plottende Graphen werden in Unterroutine erstellt
    get_plot(x, V, Emax, ew, ev, scale)

    plt.show()

if __name__ == "__main__":
    print(__doc__)
    main()

"""
Beantwortung der Fragen

Zu a)

Die Matrixgroeße N, aus welcher sich die Diskretisierungsschrittweite ergibt
wurde fuer verschiedene Werte getestet. Dabei ergab sich folgendes:
N = 50: Die Iteration ist zu gering, die Funktionen haben stark sichtbare Ecken
N = 100: Die Iteration ist zu gering. Es wird berechnet, dass ein EW ganz knapp
unter 0.25 liegt. Hier handelt es sich um einen Rundungsfehler auf Grund der
zu kleinen Berechnungszahl
N = 150: Die Iteration ist immer noch etwas klein, das Bild entspricht aber
schon eher dem zu Erwartenden.
N = 250: Es zeigt sich das zu erwartende Bild und eine gute Kurvenfuehrung im
Plot.
N >= 250. Das Bild verbessert sich nur unmerklich, daher sind keine hoeheren
Iterationen noetig.

Das betrachtete Intervall ergab sich aus der Darstellung. Es wurde so gewaehlt,
dass der wichtige Funktionsbereich mit den beiden Mulden und die wichtigen
Bereiche der Eigenfunktionen gut sichtbar sind. Bei L=1 waere ein Teil der
Eigenfunktionen abgeschnitten worden und bei L > 1.5 kommen keine weiteren
physikalisch interessanten Bereiche, die gezeigt werden muessen, daher muss
L nicht groeßer gewaehlt werden.

zu b)
Es zeigt sich das zu erwartende Bild für Eigenfunktionen. Die Funktionen sind
im allgemeinen Schwingungen, die mehr oder weniger durch den Verlauf des
Potentials veraendert werden. Fuer den untersten sichtbaren Eigenwert hat die
Eigenfunktion nur einen Extremwert und geht an den Grenzen der 2. Mulde gegen
0. In der ersten Mulde ist sie konstant, da der Eigenwert unter dem Minimum der
1. Mulde liegt. Auch beim 2.EW ist die Funktion noch konstant in der 1. Mulde,
hat aber in der 2. Mulde schon 2 Extremstellen und eine Nullstelle. Beim 3. EW
scheint die Eigenfunktion auf den ersten Blick eine aehnliche Struktur wie
die erste zu haben. Hier liegt nun in der 1. Mulde ein Maximum ohne Nullstelle
vor. In der 2. Mulde sieht sie anfangs konstant aus, aber durch Vergroeßerung
ist zu erkennen, dass auch hier eine Schwingung mit 2 Nulldurchgaengen ex.
Fuer die 3. und 4. EW kann fuer die Eigenfunktion auch noch gesagt werden, dass
im Bereich des Maximums eine geringe Aufenthaltswahrscheinlichkeit vorliegt,
die sich im 3.EW bis in die 2. Mulde erstreckt und im 4. Ew erst nach dem
Maximum steigt. Dies liegt daran, dass beide EW unter dem Wert des Maximums
liegen und daher ein Aufenthalt in nur einer der beiden Mulden am
wahrscheinlichsten ist. Ab dem 5.EW befinden sich die Funktionen ueber dem
Maximum und es ist eine Schwingung ueber den ganzen Bereich ab dem jeweiligen
Startpunkt zu beobachten. Die Funktionen werden aber alle im Bereich des
Maximums stark gestreckt. Einzig der oberste sichtbare Eigenwert weißt nahezu
keine Veraenderung im Funktionsablauf vor. Bei allen Funktionen ist auch
erkennbar, dass der Knotensatz gilt, nach welchem jede Eigenfunktion zum n-ten
Eigenwert n-1 Nullstellen hat. Wie schon bei den ersten 4 Funktionen leicht
angeschnitten, ist die Gueltigkeit bei jedem EW sichtbar. Beim 3. und 4. EW
muss allerdings die Zoom-Funktion genutzt werden, um es eindeutig zu erkennen.

Fuer groeßere heff wird die Menge der gezeigten Funktionen schnell kleiner,
wohingegen sie mit Reduzierung von heff stark zunimmt. Es zeigen sich mehr
Eigenfunktionen, die sehr schnell aufeinanderfolgen. Fuer heff = 0 zeigen sich
sehr viele Eigenwerte und -funktionen, wobei die Eigenfunktionen jeweils einer
Peakdarstellung aehneln und das Doppelmuldenpotential scheinbar in ihrem
Verlauf nachzeichnen. Fuer heff = 1 zeigen sich hingegen keine Eigenwerte und
Eigenfunktionen.

zu c)
Fuer den Fall A = 0 liegt kein asymetrisches, sondern ein symetrisches Doppel-
muldenpotential vor. Es scheinen hier immer 2er Paare von Eigenfunktionen zu
existieren, die ein bestimmtes Symmetrieverhalten aufweisen. In der ersten,
beziehungsweise auf Hoehe der ersten Mulde haben die beiden Eigenfunktionen
eine antisymmetrische Beziehung. In der 2 Doppelmulde verlaufen sie dann leicht
verschoben zueinander symmetrisch. Diese Verschiebung wird deutlicher, umso
hoeher der betrachtete Eigenwert ist. Fuer EW ~ -0.18 liegen die
Eigenfunktionen nahezu direkt aufeinander in der 2. Mulde und haben in der 1.
Mulde scheinbar an exakt der gleichen Menge ihre jeweilige Extremstelle.
Fuer das 2.Paar an Eigenfunktionen liegt schon eine leichte Verschiebung der
Phase vor, wobei die hoeher liegende Eigenfunktion ihr Maximum in der 2. Mulde
hinter der unteren hat und ihr Extremwert in der ersten Mulde leicht vor der
unteren liegt. Dieses Bild ist aehnlich fuer die hoeherliegenden Paare, wobei
sich jeweils der Abstand der Funktionen bezogen auf die y-Achse erhoeht und
die Phasenverschiebung auch immer mehr zunimmt. Im Bereich des Maximums zeigen
sich wieder andere Phaenomene. Der unterste sichtbare Energieeigenwert mit den
untersten Eigenfunktionen ist im Bereich des Maximums konstant parallel zur
x-Achse. Dies laesst darauf schließen, dass die Wahrscheinlichkeit fuer diesen
Bereich und diese Eigenfunktionen 0 ist. Die naechsthoeheren Eigenfunktionen
flachen in diesem Bereich merklich ab, erreichen aber nie eine Asymptote zur
x-Achse. Dies laesst darauf schließen, dass die Aufenthaltswahrscheinlichkeit
zwar vorhanden, aber sehr gering ist.
Das dritte Paar weißt in dem Bereich eine merkliche Verzerrung auf, allerdings
ist der grundlegende Funktionsablauf immernoch der einer (Co-)Sinusfunktion.
Da das Maximum hier ein instabiles Gleichgewicht darstellt, der EW
allerdings schon ueber der Koordinate des Maximums liegt, ist es hier nicht
unwahrscheinlich, das Teilchen anzufinden. Da es allerdings wie gesagt ein
instabiles Gleichgewicht ist und der EW nur knapp ueber dem Maximum liegt,
ist die Wahrscheinlichkeit in den Mulden dennoch sehr viel hoeher.
Das 4.Paar zeigt im Vergleich zu den anderen Paaren keine so großen
Schwankungen im Funktionsablauf. Allerdings ist hier von allen Funktionen am
deutlichsten erkennbar, dass beim Maximum alle Funktionen einmal um pi/2
verschoben werden.
"""
