#!usr/bin/pyhton
"""
Programm zum zehnten Uebungsblatt 'Ising-Modell'
"""
"""
Für die Spins: Erstelle eine Matrix mit Zufallszahlen, nimm davon %2. Die ungeraden sind up, die geraden sind down
"""
import numpy as np
import matplotlib.pyplot as plt
import functools


class IsingZustand(object):

    def __init__(self, spins, tau):
        """
        Initialisierung: ''spins'' + Temperatur ''tau''
        """

        self.spins = spins
        self.tau = tau

def click_event():
    print('Somebody clicked somewhere')

def main():

    n = 50
    tau = 2.0
    m0 = 0.5
    tsteps = 10


    ising = IsingZustand(spins, tau)
