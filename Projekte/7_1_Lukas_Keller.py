#!usr/bin/python3
"""
Programm fuer das siebte Aufgabenblatt 'Quantenmechanik von 1D-Potentialen III'
Es wird die Zeitentwicklung eines Wellenpaketes in der Husimi-Darstellung fuer
ein asymmetrisches Doppelmuldenpotential unter Abbildung des Wellenpaketes
auf kohaerente Zustaende dargestellt.
===============================================================================
Zu Beginn wird die Husimi-Funktion fuer das Wellenpaket bei t=0 und x0, p0 = 0
gezeigt. Mit einem Links-Klick in den Plotbereich kann der Startpunkt fuer das
Wellenpaket neu gewaehlt werden. Fuer diesen Startpunkt wird das Wellenpaket
dann zeitentwickelt und bis T=12 Zeiteinheiten im Phasenraum dargestellt. Dabei
werden die folgenden Parameter verwendet:
    L = 1.75, mit L Intervallgrenzen
    N = 200, mit N Matrixgroeße
    n = 70, mit n Groeße des Gitters fuer den Phasenraum
    heff = 0.07, mit heff effektives planksches Wirkungsquantum
    A = 0.15, mit A Faktor fuer Asymmetrie des Doppelmuldenpotentials
    dx = (0.5*heff)^(1/2), mit dx gleich der Peakbreite
    tmax = 12, mit tmax maximale Zeit fuer Plot
    tsteps = 10, mit tsteps Anzahl der Schritte zwischen 2 Zeiteinheiten
"""
import numpy as np
import quantenmechanik as qm
import matplotlib.pyplot as plt
import functools


def potential(x, A):
    """
    Funktion zur Berechnung des Doppelmuldenpotentials. Dabei wird x als Array
    und A als float uebergeben
    """
    return x**4 - x**2 - A*x


def calc_coeff(delta_x, ev, phi):
    """
    Berechnung der Entwicklungskoeffizienten. Dabei sind delta_x ein float und
    ev, phi Arrays.
    """
    return delta_x*np.dot(np.conjugate(np.transpose(ev)), phi)


def hamiltonian(x, p, A):
    """
    Gegebene Hamiltonfunktion mit Doppelmuldenpotential. Dabei sind x, p Arrays
    und A ein float
    """
    return (p**2)/2 + x**4 - x**2 - x*A


def coherent(dx, heff, p_0, x, x_0):
    """
    Funktion zur Berechnung der kohaerenten Zustaende in Ortsdarstellung.
    Die Berechnung erfolgt laut der Vorlesung mithilfe der Formel
    alpha(x,x0,p0) = (2pi(Dx)^2)^(-1/4)*exp(i/heff*p0x - (x-x0)^2/(4*Dx^2))
    Die Parameter sind x, x_0 und p_0 als Array und dx, heff als float
    """
    alpha_x = (2*np.pi*(dx)**2)**(-1/4) * np.exp(1j/heff * p_0 * x -
    (x-x_0)**2 / (4*(dx**2)))
    return alpha_x


def husimi_tensor(delta_x, alpha_x, phi, heff):
    """
    Funktion zur Berechnung der Husimi-Funktion. Die Berechnung erfolgt laut
    der Vorlesung mithilfe der Formel
    H(p_0, x_0) = 1/h*(sum(alpha_x* phi*Dx))^2
    Hier muss allerdings beachtet werden, dass diese Formel eine Naeherungsform
    fuer das Integral aus der Vorlesung ist, um die Berechnung mit mehrdim.
    Arrays zu vereinfachen.
    Die Parameter sind alpha_x als 3D-Array, phi als 1D-Array und heff, delta_x
    als float. Es wird hier die tensordot-Funktion genutzt, um mit dem 3D-Array
    arbeiten zu koennen. Dabei werden phi und alpha_x uebergeben. alpha_x muss
    zur Nutzung noch konjugiert werden. Der dritte Parameter 1 beschreibt die
    Achse, ueber die summiert wird, um das Matrixprodukt zu berechnen.
    """
    return np.abs(delta_x*np.tensordot(phi, np.conjugate(alpha_x), 1))**2 \
           / (heff*2*np.pi)


def gaussian(x, Dx, x_0, heff, p_0):
    """
    Definition des Wellenpaketes aus vorheriger Uebung. Es werden x als Array
    und Dx, x_0, heff und p_0 als float uebergeben.
    """
    return 1 / (2*np.pi * Dx ** 2)**(1/4) * np.exp(-(x-x_0) ** 2 /
                                                  (4 * Dx ** 2)) * \
                                                  np.exp(1j/heff * p_0 * x)

def phi_til(c, ew, t, heff, ev):
    """
    Konstruktion des zeitentwickelten Wellenpaketes bei gegebenem t. Es werden
    c, ev und ew als array, t und heff als float uebergeben.
    """
    return np.dot(ev, c*np.exp(-1j*ew*t/heff))


def click_event(event, X, heff, n, delta_x, ew, ev, c, dx,
                hus_img, alpha_x, T):
    """
    Funktion zur Verarbeitung des Klick-Events. Der Klickpunkt wird genutzt,
    um das Wellenpaket bei t=0 zu berechnen, womit die Koeffizienten und das
    zeitentwickelte Wellenpaket berechnet werden. Damit wird die
    Husimi-Darstellung unter Ausnutzung der kohaerenten Zustaende ebenfalls
    zeitentwickelt berechnet und dargestellt.
    """
    mode = plt.get_current_fig_manager().toolbar.mode
    if mode == '' and event.inaxes and event.button == 1:
        # Festlegung Startwerte
        x_0 = event.xdata
        p_0 = event.ydata
        # Erstellung Wellenpaket mit gewaehlten Startwerten
        phi = gaussian(X, dx, x_0, heff, p_0)
        # Berechnung neue Zeitentwicklungskoeffizienten
        c = calc_coeff(delta_x, ev, phi)
        # Simulation der Zeitentwicklung
        for t in T:
            # Berechnung zeitentwickeltes Wellenpaket
            phi_t = phi_til(c, ew, t, heff, ev)
            # Berechnung zeitentwickelte Husimifunktion
            h = husimi_tensor(delta_x, alpha_x, phi_t, heff)
            # Neusetzung der Werte
            hus_img.set_data(h)
            plt.gcf().canvas.flush_events()
            plt.draw()


def main():
    """
    Hauptfunktion in der die Parameter definiert werden und die notwendigen
    Arrays zur Errechnung erstellt werden, die sich zeitlich nicht aendern und
    unabhaengig vom Ausgangszustand sind. Es wird auch die Uebergabe dieser
    Werte in die weiteren Funktionen gesteuert und das Plotfenster definiert.
    """
    L = 1.75                                # Wert der Intervallgrenzen [-L,L]
    N = 200                                 # Matrixgroeße
    n = 200                                 # Gittergroeße
    heff = 0.07                             # eff. plank. Wirkungsquantum
    x_0 = 0.0                               # Wert fuer Plotbeginn
    p_0 = 0.0                               # Wert fuer Plotbeginn
    A = 0.15                                # Faktor fuer Asymmetrie
    tmax = 12                               # maximale Zeiteinheit
    tsteps = 10                             # Schritte zwischen 2 Zeitpunkten
    T = np.linspace(0,tmax, tmax*tsteps)    # Zeitenarray

    # Werte fuer Contourplot
    con_points = [-0.5, -0.4, -0.3, -0.2, -0.1, 0.0, 0.01, 0.2, 0.3, 0.4, 0.5,
                  1.0, 1.2]
    V = functools.partial(potential, A=A)   # Potential als Funktion
    dx = np.sqrt(heff/2)                    # Breite kohaerenter Zustand

    # Erzeugung der diskreten Werte unter Ausnutzung des gef. Moduls
    X, delta_x = qm.diskretisierung(-L, L, N, True)
    # Erzeugung EW und EF unter Ausnutzung des gef. Moduls
    ew, ev = qm.diagonalisierung(heff, X, V)

    # Kalkulation Wellenpaket
    phi = gaussian(X, dx, x_0, heff, p_0)
    # Kalkulation Zeitentwicklugnskoeffizienten
    c = calc_coeff(delta_x, ev, phi)
    # Kalkulation zeitentwickeltes Wellenpaket zur Zeit t=0
    phi_t = phi_til(c, ew, 0, heff, ev)

    # Arrays fuer Gittererstellung. Das p-Array wird genau entgegengesetzt dem
    # x-Array erstellt, da die imshow-Funktion auf der y-Achse den 0-Punkt oben
    # ansetzt.
    x_h = np.linspace(-L, L, n)
    p_h = np.linspace(L, -L, n)

    # Arrays fuer Contourplot
    x_c = np.linspace(-L, L, N)
    p_c = np.linspace(L, -L, N)

    # Gittererstellung fuer Contourplot
    x2d, p2d = np.meshgrid(x_c, p_c)

    # Berechnung der Hamiltonfunktion mit Gitterarrays fuer Contourplot
    H = hamiltonian(x2d, p2d, A)

    # Es werden die kohaerenten Zustaende berechnet. Dabei wird ein 3D-Array
    # erstellt. Es wird hierfuer zu erst ein mit Nullen gefuelltes Array
    # erstellt, dem der komplexe Datentyp zugewiesen wird. Dann wird ueber alle
    # x- und p-Werte der Gitterpunkte iteriert und fuer jeden Gitterpunkt der
    # Zustand ausgerechnet.
    alpha_x = np.zeros((N,n,n), dtype=np.complex128)
    for i in range(n):
        for j in range(n):
            alpha_x[:,i,j] = coherent(dx, heff, p_h[i], X, x_h[j])

    # Husimi-Funktion wird mit 3D-Array berechnet
    h = husimi_tensor(delta_x, alpha_x, phi_t, heff)


    # Allgemeine Ploteinstellungen
    plt.figure(figsize=(10,8))
    plt.title('Phasenraumdarstellung eines \n zeitentwickelten Wellenpaketes')
    plt.xlabel('Ort x')
    plt.ylabel('Impuls p')

    # Erstellung des Husimi-Plots unter Ausnutzung von imshow
    hus_img = plt.imshow(h, extent=[-1.75, 1.75, -1.75, 1.75],
                         cmap=plt.cm.gray)
    # Colorbar fuer Husimiplot
    plt.colorbar()

    # Klickfunktion als Funktion zur Weitergabe. Uebergeben werden oben
    # festgelegte Parameter
    click_function = functools.partial(click_event, X=X, dx=dx, heff=heff, n=n,
                                       delta_x=delta_x, ew=ew, ev=ev, c=c,
                                       hus_img=hus_img, alpha_x=alpha_x, T=T)
    # Verbindung Klick-Funktion und Plotbereich
    plt.connect('button_press_event', click_function)
    # Erstellung Contourplot
    plt.contour(x2d, p2d, H, con_points, linewidths=2)

    plt.show()


if __name__ == '__main__':
    print(__doc__)
    main()

"""
Diskussion der Aufgaben:
zu a)
Startet man das Wellenpaket in einem der beiden Minima des Doppelmulden-
potentials folgt dessen Bewegung dem Phasenraumplot der Hamiltonfunktion. Das
Wellenpaket kreist um das jeweilige Minima, bleibt jedoch auf einer konkreten
Bahn, die der des klassischen Falls aehnelt. Es bleibt auch lange Zeit
zusammenhaengend. Erst zum Ende der Animation hin laeuft es leicht auseinander.
Das aendert sich, wenn man das Wellenpaket weiter vom Minimum entfernt waehlt.
Innerhalb bzw. entlang der Separatrix verlaeuft das Wellenpaket langgezogener.
Außerdem ist nach einigen Zeiteinheiten zu beobachten, dass das Wellenpaket
sich aufteilt und eine Bewegung entlang der Phasenraumbahnen beider Minima
ist zu erkennen. Hier ist dann der Tunneleffekt zu beobachten. Trifft man als
Startpunkt genau den Schnittpunkt der Separatrix, teilt sich das Wellenpaket
sofort zu Beginn auf und verlaeuft entlang der Linien. Nach ungefaehr der
Haelfter der Zeiteinheiten verlaeuft das Wellenpaket sehr stark und es
entstehen mehrere Flecken, die entlang der Separatrix verlaufen.
Startet man das Paket außerhalb der Separatrix, folgt die Bewegung des
Wellenpaketes ganz kurz den Phasenraumtrajektorien. Nach einem halben Umlauf
laeuft das Paket stark auseinander und verteilt sich auf dem Plotbereich.
Dieser Effekt wird staerker, um so weiter man sich von der Separatrix entfernt.
Im Allgmeinen laesst sich beobachten, dass die Bewegungsrichtung des
Wellenpaketes immer im Uhrzeigersinn ist.

zu b)
Setzt man heff auf 0.01 erscheint das Wellenpaket kompakter, da sich die
Breite des Wellenpaketes reduziert. Sieht man sich die Bewegungen zu den oben
beschriebenen Punkten an, faellt auf, dass die Bewegungen mit reduzierten heff
aehnlich verlaufen. Da wir uns hier aber langsam dem klassischen Grenzfall
naehern, laeuft das Wellenpaket nicht so stark wie in a) auseinander und bleibt
lange Zeit kompakt. Nur vereinzelt teilt sich das Wellenpaket auf und auch dann
verlaufen die Teile groeßtenteils den Phasenraumtrajektorien. Dies ist
besonders im Bereich der Separatrix sichtbar. Es faellt allerdings auch auf,
dass fuer Energien mit dem Wert E>1 scheinbar numerische Fehler auftreten, da
das Wellenpaket nicht mehr der Trajektorie folgt, sondern den Plotbereich im
oberen Bildbereich verlaesst und unten wieder erscheint. Fuer Energien E>1.2
treten ploetzlich mehrere Wellenpakete auf, die sich nur auf und ab bewegen.
Dies laesst darauf schließen, dass die vorgenommene numerische Berechnung nur
fuer kleine Energiebereiche funktioniert.
"""
