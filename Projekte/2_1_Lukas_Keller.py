#!/usr/bin/python
'''
Programm für das zweite Aufgabenblatt 'Elementare numerische Methoden I' im
Sommersemester 2018. Es werden verschiedene numerische Ableitungsmethoden
miteinander verglichen, indem der relative Fehler zur analytischen Methode und
das zu erwartende Skalierungsverhalten gemeinsam dargestellt werden.
Die getesteten Methoden sind:

Vorwaertsdifferenz: A(x) = (f(x + h) - f(x))/h
Zentraldifferenz: A(x) = (f(x + h/2) - f(x - h/2))/h
Extrapolierte Differenz: A(x) = [8(f(x + h/4) - f(x - h/4))
                                 - f(x + h/2) - f(x - h/2))]/(3h)
=========================================================================
Die Approximationsmethoden werden auf die Funktion f(x) = arsinh(-x^2)
an der Stelle x0 = 1/5 angewandt. Es werden 1000 Iterationen durchgeführt.
'''

import numpy as np
import matplotlib.pyplot as plt

def forward_diff(func, x, h):
    '''
    Funktion für die Vorwärtsdifferenz. Diese wird auf die Stelle x0, die
    gewuenschte Funktion und alle h-Werte angewandt.
    '''
    return (func(x + h) - func(x)) / h

def central_diff(func, x, h):
    '''
    Funktion für die Zentraldifferenz. Diese wird auf die Stelle x0, die
    gewuenschte Funktion und alle h-Werte angewandt.
    '''
    return (func(x + (h/2)) - func(x - (h/2))) / h

def expo_diff(func, x, h):
    '''
    Funktion für die Extrapolierte Differenz. Diese wird auf die Stelle x0,
    die gewuenschte Funktion und alle h-Werte angewandt.
    '''
    return (8*(func(x + (h/4)) - func(x - (h/4))) -
            (func(x + (h/2)) - func(x - (h/2)))) /(3 * h)

def function(x):
    '''
    Hier wird die geforderte Funktion definiert, auf die die Approximationen
    angewandt werden sollen. Der Wert wird an der Stelle x0 berechnet.
    '''
    return np.arcsinh(-x**2)

def analytic_diff(x):
    '''
    Die analytisch berechnete Ableitung d/dx(arsinh(-x**2)) wird fuer die
    Stelle x0 zurückgegeben
    '''
    return -2*x/((x**4 + 1)**(1/2))

def calc_rel_error(x_num, x):
    '''
    Hier wird allg. der Betrag eines relativen Fehlers berechent. Bestimmt
    wird er mit der Formel delta_x = (x' - x)/x, wobei x den analytischen Wert
    und x' den numerischen Wert darstellt.
    '''
    return np.abs((x_num - x)/x)

def main():
    '''
    Hauptfunktion, in der die Variablen festgelegt und bestimmt werden und die
    gewuenschten Werte gegeneinander geplottet werden.
    '''

    x_0 = 1.0/5.0                                # Wert für abzuleitende Stelle
    N = 250000                                   # Iterationsschritte
    func = function                              # Funktion zum Übergeben

    # Fuer h wird hier ein 5 dimensionales Array erzeugt, bei dem jede
    # Zeile von h fuer ein gewünschtes h-Intervall steht.
    # h wird auf diese Weise aufgebaut, um auf die unterschiedlichen
    # Intervalle von einem Array aus zugreifen zu koennen.
    h = [10.0**np.linspace(-10.0, 0.0, N),       # h für allg. Berechnung
         10.0**np.linspace(-8.0, -0.5, N),       # h für Skal. h^1
         10.0**np.linspace(-4.5, -0.5, N),       # h für Skal. h^2
         10.0**np.linspace(-2.5, 0, N),          # h für Skal. h^4
         10.0**np.linspace(-10, -3, N)]          # h für Skal. h^(-1)


    # Bestimmung der Beträge der relativen Fehler
    rfd = calc_rel_error(forward_diff(func, x_0, h[0]), analytic_diff(x_0))
    rbd = calc_rel_error(central_diff(func, x_0, h[0]), analytic_diff(x_0))
    red = calc_rel_error(expo_diff(func, x_0, h[0]), analytic_diff(x_0))

    # Erwartetes Skalierungsverhalten für die h-Werte der einzelnen Verfahren
    # und das sichtbare h^(-1)-Verhalten. Die Formeln wurden analog zur
    # Vorlesung bestimmt und die Vorfaktoren ebenfalls analog dazu ermittelt.
    h_rfd = 0.995 * h[1]                         # O(h)
    h_rbd = 0.0067 * h[2]**2                     # O(h^2)
    h_red = 0.003 * h[3]**4                      # O(h^4)
    h_inv = 10**(-17) * h[4]**(-1)               # O(h^(-1))


    plt.figure(1, figsize=(8, 6))                # Definition des Plotfensters
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.subplot(111, xscale='log', yscale='log') # Subplot mit doppelt-log.
                                                 # Achseneinteilung

    plt.title('Numerische Differentiationsmethoden')
    plt.axis([1e-11, 1e1, 1e-18, 1e1])

    plt.xlabel(r'\textbf{Schrittweite} h')
    plt.ylabel(r'\textbf{Betrag relativer Fehler} $|\delta|$')

    # Erstellung der Plots der rel. Fehler mit Festlegung spezifischer Farben
    # fuer die einzelnen Punkte und Labelbezeichnung
    plt.plot(h[0], rfd, ls='', c='r', marker='o',
             mew=0, ms=2, label='Vorwaertsdifferenz')
    plt.plot(h[0], rbd, ls='', c='b', marker='o',
             mew=0, ms=2, label='Zentraldifferenz')
    plt.plot(h[0], red, ls='', c='g', marker='o',
             mew=0, ms=2, label='Extrapolierte Differenz')

    # h-Verhalten mit Festlegung der spezifischen Farben
    plt.plot(h[1], h_rfd, lw=3, c='r', ls='-.')
    plt.plot(h[2], h_rbd, lw=3, c='b', ls='-.')
    plt.plot(h[3], h_red, lw=3, c='g', ls='-.')
    plt.plot(h[4], h_inv, lw=3, c='k', ls='--')

    # Plotten der Graphenbeschreibung fuer das Skalierungsverhalten
    plt.text(1e-5, 1e-6, r'$\sim h$', color='r', fontsize=19)
    plt.text(1e-4, 1e-11, r'$\sim h^2$', color='b', fontsize=19)
    plt.text(1e-1, 1e-8, r'$\sim h^4$', color='g', fontsize=19)
    plt.text(1e-3, 1e-14, r'$\sim h^{-1}$', color='black', fontsize=19)

    plt.legend(loc=2, numpoints=1)               # Erstellung Legende mit einem
                                                 # Punkt als Darstellung und
                                                 # Positionierung links oben
    plt.show()


if __name__ == '__main__':
    print(__doc__)
    main()

'''
Beantwortung der Fragen

zu a)
Das h^(-1) Verhalten der Methoden entsteht vermutlich durch Rundungsfehler,
die automatisch entstehen. Da die numerischen Methoden schon eine kleine
Abweichung zur analytischen Berechnung bewirken, kommen diese Rundungsfehler
dann vor allem für sehr kleine h Werte wegen des Faktors 1/h in den Methoden
besonders zum Tragen. Außerdem fallen die Rundungsfehler bei sehr kleinen h
viel staerker aus, wodurch mit Erhoehung von h der Rundungsfehler immer mehr
abnimmt. Wenn h dann in einem Bereich ist, in dem das erwartete Skalierungs-
verhalten zu erkennen ist, ueberwiegt der numerisch zu erwartende Fehler die
durch die mechanische Berechnung entstehenden Rundungsfehler und behindern das
vorher beobachtete Verhalten.

zu b)
In der Tabelle werden die optimalen Werte für h und die dazu jeweils
berechneten relativen Fehler aufgefuehrt. Dies sind jeweils die h-Werte fuer
die minimalsten Fehler und wuerden dadurch bei Verwendung der
Approximationsmethode die geringste Abweichung von der analytischen Loesung
bewirken. Es ist hier deutlich zu sehen, dass die extrapolierte Differenz
bei rel. großen h die geringste Abweichung verursacht.
                        |          h         |      rel. Fehler
------------------------|--------------------|----------------------
Vorwaertsdifferenz      |         1.34e-9    |       2.4e-10
------------------------|--------------------|----------------------
Zentraldifferenz        |         1.21e-5    |       7.75e-14
------------------------|--------------------|----------------------
Extrapolierte Differenz |         5e-4       |       2.74e-15

Allerdings gelten diese h-Werte nur für die gewaehlten Iterationsschritte und
die gewaehlte Funktion und variieren stark, wenn man die Zahl der
Iterationsschritte oder die Funktion aendert. Es waere daher vermutlich
sinnvoller, Werte zu nehmen, die in einem Bereich liegen, der in Bezug auf die
getroffene Wahl nur schwach varriert. Dieser sollte dann vermutlich eher so
gewaehlt werden, dass ein h-Wert aus dem niedrigsten Bereich der Werte gewaehlt
wird, in dem das erwartete Skalierungsverhalten noch erkennbar ist. Dies wuerde
die folgenden Werte ergeben

                        |          h         |      rel. Fehler
------------------------|--------------------|----------------------
Vorwaertsdifferenz      |         7.82e-9    |       1.87e-8
------------------------|--------------------|----------------------
Zentraldifferenz        |         1.85e-5    |       5.83e-12
------------------------|--------------------|----------------------
Extrapolierte Differenz |         2.3e-3     |       1.64e-13

Dies entspricht auch eher den zu erwartenden Fehlerbereichen, die in der
Vorlesung ermittelt wurden.
'''
