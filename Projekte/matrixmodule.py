#!usr/bin/python3
"""
Matrixmodule soll in ein die 5_1 rein
"""
import numpy as np
from scipy.linalg import eigh
import matplotlib.pyplot as plt

def create_discrete_values(N, xmax, xmin, cal_delta=True):
    if N != 1:
        delta_x = (xmax - xmin)/(N - 1)
        x, h = np.linspace(xmin + delta_x, xmax - delta_x, N, retstep=True)
        return x, h
    else:
        raise ValueError('N sollte immer groeßer 1 sein!')


def get_eigen(x, delta_x, N, heff, V):

    z = (heff**2)/(2.0*(delta_x)**2)

    matrix = np.diag(V + 2*z)
    i, j = np.indices(matrix.shape)
    matrix[i==j-1] = -z
    matrix[i==j+1] = -z

    ew, ev = eigh(matrix)

    return ew, ev

def get_plot(x, V, Emax, ew, ev, scale):

    plt.axis([np.min(x), np.max(x), np.min(V), Emax])
    plt.plot(x, V)

    for i in range(len(ew)):
        if ew[i] > Emax:
            length = i
            break

    for i in range(length):
        plt.plot(x, ew[i] + np.zeros(len(x)))
        plt.plot(x, ew[i] + scale*(ev[:, i]))

    plt.show()
