#!usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matrixmodule
from scipy.misc import derivative
import math


def potential(x):
    return 0.5*x**2



def harm_osci():
    xmax = 5.0
    xmin = -5.0
    N = 200
    heff = 1.0
    Emax = 5

    x, delta_x = matrixmodule.create_discrete_values(N, xmax, xmin)

    V = potential(x)
    ew, ev = matrixmodule.get_eigen(x, delta_x, N, heff, V)

    matrixmodule.get_plot(x, V, Emax, ew, ev, 0.5)


harm_osci()
