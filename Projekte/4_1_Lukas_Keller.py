#/usr/bin/python3

"""
Programm fuer das vierte Aufgabenblatt 'Differentialgleichungen' im Sommersem-
ester 2018. Es wird die Dynamik eines Teilchens, welche durch die Hamiltonfkt.
im angetriebenen Doppelmuldenpotential V(x,t) = x^4-x^2+x(A+Bsin(omega*t))
mit A=0.2 und B=0.1, sowie omega=1 fuer 200 Perioden untersucht. Dafuer werden
die kanonischen Gleichungen geloest und die Trajektorie und die Phasenraum-
punkte jeweils in einem Plotbereich dargestellt, sowie Konturlinien fuer H
dargestellt.
===============================================================================
Durch einen Klick in die Plotfenster mit der linken Maustaste lassen sich die
Startwerte auswaehlen, fuer die man die Trajektorie berechnen moechte.
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint as integrate     # Integralberechnung
import functools

def differential(f, t, A, B, omega):
    """
    Berechnung der zeitlichen Ableitung der gegebenen Hamiltonfkt unter
    Ausnutzung der kanonischen Gleichungen mithilfe eines 2-Werte Arrays f, an
    dem die Ableitung ausgewertet werden soll, eines Arrays mit Zeitwerten t
    und zwei floats, den Parametern A, B.
    """
    q, p = f                                        # Aufspaltung Vektor in
                                                    # beide Werte

    # Mit kanonischen Gleichungen wird zeitliche Ableitung berechnet
    dfdt = [p, - (4.0*q**3.0 - 2.0*q + A + B*np.sin(omega*t))]
    return dfdt

def hamiltonian(q, p, A, B, omega, t):
    """
    Gegebene Hamiltonfunktion mit Doppelmuldenpotential. Bei den Parametern
    handelt es sich um 2 2D-Arrays q, p und 4 Parameter A, B, omega und t als
    floats
    """
    return (p**2)/2 + q**4 - q**2 + q*(A + B*np.sin(omega*t))

def click_event(event, omega, A, B, t, phaseplot, stroboplot,
                strob_step):
    """
    Funktion, die bei Mausklick mit der linken Taste in den Plotbereich einen
    Startpunkt fuer die Berechnung der Trajektorie ausgibt und mit diesem die
    gesuchte Trajektorie berechnet, sowie diese Werte als Trajektorie im
    Phasenraum bzw. als stroboskopische Phasenraumdarstellung plottet.
    """
    # Nur linker Mausklick im Plotfenster ist moeglich. Zoomen erzeugt außerdem
    # keinen neuen Kalkulationszyklus
    mode = plt.get_current_fig_manager().toolbar.mode
    if mode == '' and event.inaxes and event.button == 1:
        # Festlegung der Startwerte durch Klickpunkt
        x_0 = event.xdata
        y_0 = event.ydata

        # Speicherung Startwerte in 2D-Startpunkt
        f_0 = np.array([event.xdata, event.ydata])

        # Berechnung der Werte im Phasenraum fuer gegebene Zeiten mithilfe der
        # odeint-Funktion von scipy, die oben in integrate umbenannt wurde, um
        # darzustellen, was mit den Werten aus der differential-Funktion
        # passiert
        f = integrate(differential, f_0, t, args=(A, B, omega))
        # Aufteilung des Phasenraumarrays in 2 Arrays fuer die jeweiligen
        # Werte fuer die Orte und die Impulse
        q = f[:, 0]                                   # Ortarray
        p = f[:, 1]                                   # Impulsarray

        # Das Array fuer die stroboskopische Darstellung wird aus dem oberen
        # Werte-Array gebildet, in dem die Schrittweite gewaehlt wird, in der
        # die ganzzahligen Vielfachen von 2pi auseinanderliegen. Dann wird
        # das jeweils in der Schrittweite naechstliegende Element ausgewaehlt
        # und in dem Array abgespeichert.
        f_strob = f[::strob_step]

        # Stroboskopwerte fuer Orte und Impulse
        q_strob = f_strob[:, 0]                       # Stroboskop-Ortsarray
        p_strob = f_strob[:, 1]                       # Stroboskop-Impulsarray

        # Plotten der Trajektorie im Phasenraum
        phaseplot.plot(q, p, ls='-', lw=2)

        # Plotten der stroboskopischen Darstellung
        stroboplot.plot(q_strob, p_strob, ls='', marker='o', ms=2)
        plt.draw()


def main():
    """
    Hauptfunktion, in der die noetigen Parameter definiert werden, sowie die
    Periodendauer und das Zeitenarray berechnet werden. Außerdem wird der
    Bereich fuer den contour-Plot gewaehlt und die noetigen Werte fuer den
    contour-Plot berechnet. Es wird auch das allgemeine Plotfenster erstellt.
    """

    # Festlegung der Parameter fuer das Doppelmuldenpotential
    A = 0.2
    B = 0.1
    omega = 1.0

    # Parameter fuer Zeitberechnung
    num_per = 200                                      # Periodenanzahl
    num_it = 500                                       # Anzahl der Iterationen
                                                       # pro Periode
    T = 2*np.pi/omega                                  # Periodendauer
    t = np.linspace(0.0,num_per*T,num_per*num_it + 1)  # Berechnung der
                                                       # Zeitwerte fuer num_per

    # Festlegung der Grenzen zum Plot
    q_lim = 1.5                                        # Grenze fuer Ort
    p_lim = 2.0                                        # Grenze fuer Impuls
    N = 1000                                           # Iterationzahl fuer
                                                       # contour-plot

    # Auswahl der geeigneten Stellen, um 8 Energieplots darzustellen
    con_points = [-0.3, -0.2, -0.15, -0.05, 0.01, 0.4, 1.3, 1.6]

    # Ortarray fuer contour-Plot
    q_H = np.linspace(-q_lim, q_lim, N)
    # Impulszahl fuer contour-Plot
    p_H = np.linspace(-p_lim, p_lim, N)
    # Erzeugung von 2D-Arrays fuer contourplot
    q_mesh, p_mesh = np.meshgrid(q_H, p_H)
    # Berechnung der Energiewerte mithilfe der Hamiltonfunktion
    H = hamiltonian(q_mesh, p_mesh, A, B, omega, 0)


    # Definition der Plotfenster
    fig = plt.figure(figsize=(14, 8))

    # Plotfensterdefinition fuer Trajektorie im Phasenraum mit contour-Plot
    phaseplot = plt.subplot(121)
    phaseplot.set_title('Trajektorie im Phasenraum')
    phaseplot.set_xlabel('Ort q')
    phaseplot.set_ylabel('Impuls p')
    phaseplot.axis([-q_lim, q_lim, -p_lim, p_lim])
    phaseplot.contour(q_mesh, p_mesh, H, con_points, colors='black',
                      linestyles='dashed')

    # Plotfensterdefinition fuer stroboskopische Darstellung mit contour-Plot
    stroboplot = plt.subplot(122)
    stroboplot.set_title('Stroboskopische Darstellung')
    stroboplot.set_xlabel('Ort q')
    stroboplot.set_ylabel('Impuls p')
    stroboplot.axis([-q_lim, q_lim, -p_lim, p_lim])
    stroboplot.contour(q_mesh, p_mesh, H, con_points, colors='black',
                       linestyles='dashed')

    # Aufrufen der click-Funktion zur Interaktion mit Plotbereich
    click_function = functools.partial(click_event, omega=omega, A=A, B=B, t=t,
                                       phaseplot=phaseplot,
                                       stroboplot=stroboplot,
                                       strob_step=num_it)
    # Integration der click-Funktion in den Plotbereich
    plt.connect('button_press_event', click_function)
    plt.show()


if __name__ == '__main__':
    print(__doc__)
    main()

"""
Beantwortung der Aufgaben

Zum Teil a) mit B = 0

Wenn B = 0, ist das Potential ein Doppelmuldenpotential, bei dem ein Minimum
etwas tiefer liegt als das andere. Je nach Startenergie ergeben sich
verschiedene Bewegungen im Orstraum. Fuer niedrige Energien mit E_1<0 ergeben
sich Schwingungen um das erste Minimum. Diese werden im Phasenraum als
Ellipse dargestellt, deren Mittelpunkt auf dem gleichen q-Wert liegt, wie das
erste Minimum. Es ergeben sich auch fuer Energien mit E_1<E<0 Schwingungen um
das zweite Minimum. Hier ergibt sich im Phasenraum ebenfalls eine Ellipse,
deren Mittelpunkt auf dem gleichen q-Wert liegt, wie das zweite Minimum.
Bei Energien mit E~0 ist eine Schwingung um eine der beiden Minima moeglich,
je nachdem, wohin das instabile Gleichgewicht verschoben wird. Im Phasenraum
ergibt sich die Darstellung der Separatrix, also einer umgekippten Acht um die
vorherigen Ellipsen.
Fuer E>0 ergeben sich Schwingungen ueber den gesamten Potentialbereich, so dass
je nach Wahl der Startenergie das jeweilige Ortsintervall mit beiden Minima
durchlaufen wird. Im Phasenraum sind das geschlossene Kurven, die die
Separatrix umhüllen. Sie aehneln also einer geschlossenen Kurve, die auf der
oberen und unteren Seite zum Zentrum hin konkav gebeugt sind.

Zeim Teil b) mit B = 0.1

Hier liegt ein verglichen mit Aufgabenteil a) leicht verändertes Potential vor.
Fuer kleine Energien liegen wie im Aufgabenteil a) Schwingungen in beiden
Minima um die jeweiligen Gleichgewichtslagen vor. Diese sind nahezu periodisch
und druecken sich wieder als Ellipsen im Phasenraum aus. Fuer groeßere
Energien wird das System chaotischer, es zeigen sich im Bild Bereiche um die
anfaenglichen kleinen Ellipsen, in denen chaotische Dynamik erkennbar ist.
Diese Bereiche liegen wie Inseln um die Ellipsen. Sieht man sich noch groeßere
Energien im Bereich E=0 bis E=0.4 an, bilden sich aehnlich zum Aufgabenteil a)
wieder offene umgekippte Achten als Trajektorien aus.
Im Gegensatz zum vorherigen Aufgabenteil allerdings sind diese keine feinen
Linien sondern unregelmaeßiger. Nimmt man die stroboskopische Darstellung hinzu
ist erkennbar, dass in diesem Bereich chaotische Dynamik vorliegt. Es ist
im Phasenraum deutlich erkennbar, dass die Trajektorien sehr wechselhaft sind
und sich auch ueberschneiden koennen. Dies war in Aufgabenteil a) nicht
moeglich.Fuer Werte im Phasenraum um (0.0, -0.5) deutet sich aber wieder
eine groeßere Insel mit periodischer Dynamik an.
Nimmt man Energien mit E>=0.4 sind die offenen Achten wieder regelmaeßiger.
Auch in der stroboskopischen Darstellung sind wieder klare Bahnen zu erkennen.
Fuer diesen Bereich liegen also wieder nahezu periodische Schwingungen vor, die
das gesamte Potential ausnutzen.
Es ist zu vermuten, dass eine weitere Vergroeßerung von B dazu fuehren wuerde,
dass das System immer chaotischer wird und die kleineren Inseln, die erkennbar
waren, sich in die Bereiche hoeherer Energie bewegen, waehrend neue dazukommen.

Zum Teil c)
Fuer die Startkoordinaten p=0.0596 and q=-0.7262 ergibt sich eine Periode von
t* = 69.165 Zeiteinheiten, was einer Schwingungszahl n=11.008 entspricht.
Fuer die geg. Startkoordinaten liegt also eine Periode von rund 11 Schwingungen
vor.
Zum Finden wurde eine Unterroutine genutz, die den Startwert der Trajektorie
mit allen folgenden Werten vergleicht. Wenn diese bis auf eine gewisse erlaubte
Ungenauigkeit von delta = 0.0001 aufeinanderliegen, wird der Wert fuer t
zurueckgegeben.
Dafuer wurden folgende Codezeilen an der Zeile 70 eingefuegt:

delta = 0.001

for i in range(1,len(q)):
    if q[0] > q[i] - delta and q[0] < q[i] + delta:
        if p[0] > p[i] - delta and p[0] < p[i] + delta:
            print('For p=',p[0],'and q=',q[0],'one finds a period of')
            print(i, t[i]/(2*np.pi))
            break
"""
