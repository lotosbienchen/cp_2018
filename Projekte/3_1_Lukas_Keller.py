#/usr/bin/python3
"""
Programm fuer das dritte Aufgabenblatt 'Elementare numerische Methoden II' im
Sommersemester 2018. Es werden 3 numerische Integrationsmethoden miteinander
verglichen, in dem der Betrag des relativen Fehlers zur analytischen Methode
und das zu erwartende Skalierungsverhalten gemeinsam dargestellt werden.
Es werden die Mittelpunktmethode, Trapezmethode und Simpsonregel getestet.
===============================================================================
Getestet werden die numerischen Methoden an der Funktion f(x) = sinh(2x) in den
Grenzen -pi/2 bis pi/4 auf einem Bereich von N Teilintervallen, so dass min.
ein Bereich von h in [10e-4,1] abgedeckt ist.
"""
import math                                          # Fuer error-Funktion
import numpy as np
import matplotlib.pyplot as plt

def numeric_rules(method, func, x_begin, x_end, N):
    """
    Funktion zur Berechnung der Stuetzstellen und Schrittweiten anhand
    gegebener Integrationsgrenzen sowie der Iterationsanzahl N. Mithilfe der
    Uebergabe eines Keywords kann die gewuenschte Approximationsmethode
    gewaehlt werden und die gewuenschten Stuetzstellen, sowie die Schrittweite
    werden an die gewaehlte Integrationsfunktion uebergeben. Bei den Parametern
    handelt es sich um 'method' und 'func' als string, x_begin und x_end als
    floats fuer die Festlegung der Grenzen und N als integer.
    """
    # Berechnung der Stuetzstellen und der Schrittweite, dabei wird in 2 Faelle
    # unterteilt, da linspace dies erfordert
    if N == 1:
        # Fuer N = 1 gibt die linspace-Option retstep einen Fehler aus, wenn
        # man sich h zurueckgeben lassen will. Daher wird dieser Fall einzeln
        # berechnet.
        x = np.linspace(x_begin, x_end, N)
        h = x_end - x_begin
    else:
        # Fuer N !=1 wird retstep verwendet, um h zu bestimmen.
        x, h = np.linspace(x_begin, x_end, N, retstep=True, endpoint=False)

    # Wahl der Integrationsmethode
    if method == 'midpoint':
        return midpoint(func, x, h)
    elif method == 'trapezoid':
        return trapezoid(func, x, h)
    elif method == 'simpson':
        return simpson(func, x, h)

def midpoint(func, x, h):
    """
    Mittelpunktmethode zur Berechnung des Integrals der Funktion 'func' auf dem
    Intervall [a,b] mit der Schrittweite h. Bei den Parametern handelt es sich
    um 'func' als string zur Uebergabe in die functions-Funktion, ein
    x-Werte Array und h als float
    """
    return np.sum(h*functions(func, x + h/2))

def trapezoid(func, x, h):
    """
    Trapezmethode zur Berechnung des Integrals der Funktion 'func' auf dem
    Intervall [a,b] mit der Schrittweite h. Bei den Parametern handelt es sich
    um 'func' als string zur Uebergabe in die functions-Funktion, ein
    x-Werte Array und h als float
    """
    return np.sum(h*(functions(func, x) + functions(func, x + h))/2)

def simpson(func, x, h):
    """
    Simpsonregel zur Berechnung des Integrals der Funktion 'func' auf dem
    Intervall [a,b] mit der Schrittweite h. Bei den Parametern handelt es sich
    um 'func' als string zur Uebergabe in die functions-Funktion, ein
    x-Werte Array und h als float
    """
    return np.sum(h/6*(functions(func, x + h) + 4*functions(func, x + h/2) +
                       functions(func, x)))

def functions(functionname, x):
    """
    Anlegen eines Funktionskatalogs, wodurch die gewuenschte Funktion mithilfe
    der Uebergabe eines Keywords ausgewaehlt werden kann. Es kann zwischen
    4 Funktionen gewaehlt werden:
        1. f(x) = sinh(2x)
        2. f(x) = exp(-100*x^2)
        3. f(x) = (1 + sign(x))/2
        4. f(x) = 1
    Bei den Parametern handelt es sich mit 'functionname' um einen string und x
    um ein Array aus floats fuer welches die Funktionswerte ermittelt werden
    sollen
    """

    if functionname == 'function1':
        func = np.sinh(2*x)
    elif functionname == 'function2':
        func = np.exp(-100.0*(x)**2.0)
    elif functionname == 'function3':
        func = 0.5 * (1.0 + np.sign(x))
    elif functionname == 'function4':
        func = 1.0

    return func

def integrals(integral, a, b):
    """
    Ein zu den Funktionen gehoeriger Integralkatalog, in dem die analytischen
    Integrale zu den einzelnen Funktionen abgerufen werden koennen. Analog zu
    obiger Funktion erfolgt die Wahl wieder durch Keywords, die die gleichen
    Keywords, wie die der functions-Funktion sind. Die Integrale werden fuer
    die gegebenen Grenzen ermittelt. Bei den Parametern handelt es sich um
    'integral' als string und a und b als floats für die Integrationsgrenzen
    """
    if integral == 'function1':
        # Analytisches Integral fuer f(x) = sinh(2x)
        func_int = (1/2)*np.cosh(2*b) - (1/2)*np.cosh(2*a)
    elif integral == 'function2':
        # Analytisches Integral fuer f(x) = exp(-100*x^2)
        func_int = (1/20) * np.sqrt(np.pi) * (math.erf(10*b) - math.erf(10*a))
    elif integral == 'function3':
        # Analytisches Integral fuer f(x) = (1+sign(x))/2
        func_int = 0.5 * ((-abs(a) - a + abs(b) + b))
    elif integral == 'function4':
        # Analytisches Integral fuer f(x) = 1
        func_int = b - a

    return func_int

def main():
    """
    Hauptfunktion mit Festlegung der Variablen, Berechnung des rel. Fehlers
    und Plotten der gewuenschten Daten
    """
    a = np.pi/(-2)                                  # untere Integrationsgrenze
    b = np.pi/4                                     # obere Integrationsgrenze

    func = 'function1'                              # Funktion, auf die Meth.
                                                    # angewandt werden
                                                    
    methods = ['midpoint', 'trapezoid', 'simpson']  # Methodenliste

    N = 1000                                        # Gesamtzahl Teilintervalle

    # Berechnung der Teilintervalle als Integer, um gewuenschten h-Bereich zu
    # realisieren. Das resultiert am Ende in einer red. Gesamtzahl von n_len,
    # da mit np.unique alle wegen der Rundung doppelt auftretenden Zahlen
    # rausgenommen werden.
    n = np.unique(np.int32(10**np.linspace(0.0, 5.0, N)))
    n_len = len(n)
    h = (b-a)/n                                     # Berechnung des h-Arrays


    # Analytische Integralwerte fuer gewuenschte Funktion zu geg. Grenzen
    func_int = integrals(func, a, b)

    data_mp = np.zeros(n_len)                       # Array fuer Mittelpunktm.
    data_trap = np.zeros(n_len)                     # Array fuer Trapezmeth.
    data_sim = np.zeros(n_len)                      # Array fuer Simpsonregel

    # Berechnung der Werte fuer die 3 Methoden und Speicherung in oben gebauten
    # Arrays
    for i in range(n_len):
        # Werte Mittelpunktmethode
        data_mp[i] = numeric_rules(methods[0], func, a, b, n[i])
        # Werte Trapezmethode
        data_trap[i] = numeric_rules(methods[1], func, a, b, n[i])
        # Werte Simpsonregel
        data_sim[i] = numeric_rules(methods[2], func, a, b, n[i])

    # Berechnung Betrag des relativen Fehlers zum analytischen Wert
    err_mp = np.abs((data_mp - func_int)/func_int)       # Mittelpunktfehler
    err_trap = np.abs((data_trap - func_int)/func_int)   # Trapezfehler
    err_sim = np.abs((data_sim - func_int)/func_int)     # Simpsonfehler

    # Definiere Variablen, um Intervallgrenzen fuer die reduzierten h-Arrays,
    # die fuer das Skalierungsverhalten unten definiert werden an einer Stelle
    # festlegen zu koennen
    j = 15                                          # Anfangsindex
    k = 600                                         # Endindex fuer Mittelpunkt
                                                    # und Trapezmethode
    l = 350                                         # Endindex fuer Simpson

    # Einschraenkung des h-Arrays fuer das Plotten des Skalierungsverhalten
    h_mp = h[j:k]                                   # h-Werte für Mittelpunkt
    h_trap = h[j:k]                                 # h-Werte für Trapezmeth.
    h_sim = h[j:l]                                  # h-Werte für Simpsonregel


    # Definieren des allg. Plotfensters und LaTex-Implementierung
    plt.figure(1, figsize=(10, 8))
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    # Subploteinstellungen und -titel
    plt.subplot(111, xscale='log', yscale='log')
    plt.title('Numerische Integrationsmethoden')

    plt.xlabel(r'\textbf{Schrittweite} h')
    plt.ylabel(r'\textbf{Betrag relativer Fehler} $|\delta|$')

    # Plotten des Verhaltens des Betrags des rel. Fehlers ueber alle h-Werte
    plt.plot(h, err_mp, ls='', marker='.', ms='4', c='r',
             label='Mittelpunktmethode')
    plt.plot(h, err_trap, ls='', marker='.', ms='4', c='b',
             label='Trapezmethode')
    plt.plot(h, err_sim, ls='', marker='.', ms='4', c='g',
             label='Simpsonregel')

    # Plotten des zu erwartenden Skalierungsverhaltens ueber die vorher
    # reduzierten h-Werte. Die Vorfaktoren wurden dabei so verwendet, dass
    # die einzelnen Skalierungsverhalten gut zu erkennen sind.
    plt.plot(h_mp, 0.05*h_mp**2, ls='-.', c='r', lw=3,
             label=r'$\sim\mathcal{O}(h^2)$')
    plt.plot(h_trap, h_trap**2, ls='-.', c='b', lw=3,
             label=r'$\sim\mathcal{O}(h^2)$')
    plt.plot(h_sim, 0.001*h_sim**4, ls='-.', c='g', lw=3,
             label=r'$\sim\mathcal{O}(h^4)$')

    plt.legend(loc=4, numpoints=1)
    plt.show()

if __name__ == '__main__':
    print(__doc__)
    main()

"""
Beantwortung der Fragen

Zur analytischen Berechnung der integrale

a) int(sinh(2x))dx von -pi/2 bis pi/4 = 1/2*cosh(2x) von -pi/2 bis pi/4
                                      = 1/2(cosh(pi/2) - cosh(-pi))
                                      = -4.5414 (gerundet)

b) int(exp(-100*x^2))dx = 1/20*(pi)^(1/2)*erf(10*x) von -pi/2 bis pi/4
                        = 1/20*(pi)^(1/2)*(erf(5/2*pi) + erf(5*pi))
                        = 0.177245 (gerundet)

c) int(1/2*(1+sign(x))) = 1/2*x(sign(x) + 1) von -pi/2 bis pi/4
                        = 0.7854

Zu den Ergebnissen der Integrationsmethoden fuer die 3 Funktionen

Für Funktion a)
Die 3 getesteten Methoden zeigen das Skalierungsverhalten auf, was in der
Theorie auch erwartet werden wuerde. Einzig bei der Simpsonmethode gibt es bei
kleinen h-Werten im Bereich von 10e-4 Abweichungen von dem eigentlich
erwarteten Bild. Die relativen Fehler zeigen hier ein diskretes Verhalten auf,
so dass sich leicht erkennbare zur x-Achse paralelle Linien bilden. Erst im
Bereich von 10e-3 gehen die Linien ueber in das erwartete Verhalten, was durch
die doppeltlogarithmische Darstellung linear verlaufen sollte und ab da auch
tut.
Fuer die Mittelpunktmehtode und die Trapezmethode resultiert dieses Bild aus
dem Verhalten der Methode mit Vergroeßerung der Schrittweite. Fuer kleine
Schrittweiten h stellen die beiden Methoden gute Approximationen der Funktion
bereit, so dass der Betrag des relativen Fehlers gegenueber dem analytischen
Wert klein ausfaellt. Vergroeßert sich der h-Wert, nimmt die Qualitaet der
Approximation immer mehr ab, so dass der relative Fehler stetig zunimmt. Dies
drueckt sich wie erwartet in unserer Darstellung als linearer Anstieg aus.
Die Ursache fuer die Groeße des Betrags des relativen Fehlers ist bei diesen
beiden Methoden also die Verschlechterung der Approximation, was ich als
Methodenfehler bezeichnen wuerde. Rundungsfehler kommen zwar auch zum Tragen,
sind aber im Vergleich zum Fehler der Approximation nicht so ausschlaggebend.
Die Simpsonmethode ermoeglicht im Allgemeinen eine genauere Approximation und
hat daher für geringe Schrittweiten h auch sehr geringe relative Fehler, die
aus der Methode an sich entstehen. Das diskrete Verhalten der Fehlerwerte
im Intervall [10e-4,10e-3] wird daher massiv durch Rundungsfehler gepraegt, was
vor allem die Diskretisierung erklaert. Es wird hier, auch wegen der Verwendung
von Integer bei dem n-Array, auf wenige feste Fehlerwerte maschinell gerundet,
so dass diese Werte alle auf einer Linie liegen. Die Rundungsfehler werden noch
deutlicher, weil die Groeße der Teilintervallzahl die Anzahl an Werten erhoeht,
die auf den gleichen Betrag des relativen Fehlers gerundet werden.
Für groeßere Schrittweiten ist der moegliche Rundungsbereich ebenfalls groeßer
und die Rundungsfehler nehmen ab. Allerdings nehmen an dieser Stelle wie auch
schon bei den beiden anderen Methoden die Fehler, die aus der Methode
resultieren zu. Dadurch entsteht bei hoeheren h-Werten das erwartete
Skalierungsverhalten von h^4, was in unserer Darstellung ebenfalls wieder ein
lineares, aber staerker ansteigendes Verhalten anzeigt.

Für Funktion b)
Die Funktion b) ist wie auch im Aufgabenblatt beschrieben eine Funktion, die
an den Grenzen approximativ verschwindet. Die Funktion stellt eine stark
gestauchte Gaussfunktion dar und aehnelt in ihrem Aussehen einer delta-
Funktion. Sie besitzt also einen stark ausgepraegten Peak.
Auffaellig ist bei dieser Funktion, dass keine der drei Methoden das erwartete
Skalierungsverhalten zeigt. Alle drei weisen fuer eine Schrittweite h im
Intervall von [10e-4,10e-2] eine Parallelitaet zur x-Achse auf, der Fehler
veraendert sich also nicht und ist im Bereich von ca. 10e-15. Die 3 Methoden
ermoeglichen also fuer kleine Schrittweiten eine gute Approximation der
Funktion. Der relative Fehler wird in diesem Bereich auch aehnlich zur
Simpsonregel bei Funktion a) stark von Rundungsfehlern gepraegt, die wie oben
schon beschrieben zu einer Diskretisierung der Fehler fuehren, was wieder auf
die maschinelle Rundung zurueckzufuehren ist. Fuer groeßere Schrittweiten
steigt das Fehlerverhalten aller drei Methoden nahezu im gleichen Maße
schlagartig an und auch hier zeigt sich nicht das erwartete
Skalierungsverhalten. Dies wird darauf zurueckzufuehren sein, dass die
Approximation aller 3 Methoden bei groeßeren h nicht ausreicht, um das Peak-
Verhalten gut naehern zu koennen, so dass die Approximationen sozusagen ueber
den Peak hinausschießen und dadurch die Fehler stark zunehmen.

Für Funktion c)
Die Funktion c) ist laut Aufgabenblatt die unstetige Heaviside-Funktion. Sie
stellt die konstante 0-Funktion bis zum Punkt (0,0) dar, ab welchem sie auf die
konstante 1-Funktion springt.
Auch bei dieser Funktion faellt auf, dass das erwartete Skalierungsverhalten
fuer die 3 Methoden nicht erkennbar ist, was dadurch begruendet werden kann,
dass die in der Vorlesung vorgenommene Herleitung des Skalierungsverhaltens
nicht auf unstetige Funktionen angewendet werden kann. Weiterhin faellt auf,
dass alle 3 Methoden einen Fehlerbereich aufweisen, der jeweils in 2 Linien/
Bereiche, aufgeteilt ist. Die Simpsonregel und die Trapezmethode weisen dabei
jeweils 2 zueinander parallele Linien auf, die etwas versetzt sind. Die
Mittelpunktmethode hingegen, weißt eine Linie auf, die parallel zu denen der
Simpsonregel und der Trapezmethode ist und einen diskreten Bereich im unteren
Bildbereich. Dieser diskrete Bereich weist zur x-Achse parallele Linien auf.
Das Verhalten laesst sich dadurch erklaeren, dass die Hauptfehlerursache in dem
Bereich um die Sprungstelle, also um (0,0) liegt. Die Trapezmethode und die
Simpsonregel lassen hierbei einen methodischen Fehler entstehen, der nahezu
linear mit dem zunehmen von h ansteigt. Rundungsfehler spielen bei den beiden
Methoden allerdings keine wichtige Rolle. Bei der Mittelpunktmethode laesst
sich das Verhalten der zu den beiden anderen Methode parallel verlaufenden
Linie analog erklaeren. Der diskrete Fehlerbereich im unteren Bild entsteht
vermutlich dadurch, dass die Approximation der Quadrate genau das Verhalten
der Heaviside-Funktion wiederspiegelt und daher keine Fehler der Methode
aufweisen wuerde. Hier kommen also nur die maschinellen Rundungsfehler zum
Tragen, die oben schon erlaeutert wurden.
"""
